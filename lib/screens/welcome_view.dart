import 'dart:async';

import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/material.dart';
import 'package:garda_digital/global.dart';
import 'package:garda_digital/material/firebase.dart';
import 'package:garda_digital/screens/dashboard.dart';

final List<String> imgList = [
  'assets/images/hi.png',
  'assets/images/hand.png',
  'assets/images/memberi.png',
  'assets/images/respect.png',
];
final List<String> wordingTopList = [
  '',
  'PEDULI',
  'MEMBERI',
  'MENJAGA',
];
final List<String> wordingBotList = [
  'Halo, Selamat bergabung di',
  'Mendengar, menampung dan',
  'Memberi dengan kegiatan nyata',
  'Menjaga dan mempererat rasa',
];

final List<String> wordingBot2List = [
  'Garda Digital!',
  'merencanakan gagasan.',
  'baik online maupun offline.',
  'saling memiliki.',
];

class WelcomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _welcomePage();
  }
}

class _welcomePage extends State<WelcomePage> {
  bool _isLoading = true;
  _paddingTop(var _mediaQuery) {
    var result;
    if (_mediaQuery.size.height < 650) {
      result = _mediaQuery.size.height / 18;
    } else if (_mediaQuery.size.height > 650 && _mediaQuery.size.height < 860) {
      result = _mediaQuery.size.height / 10;
    } else {
      result = _mediaQuery.size.height / 10;
    }
    return result;
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    new Timer(new Duration(seconds: 1), () {
      SessionLogin.cek().then((_cekSession) {
        if (_cekSession == true) {
          Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(
                  builder: (BuildContext context) => DashboardPage()),
              (Route<dynamic> route) => false);
        } else {
          setState(() {
            _isLoading = false;
          });
        }
      });
    });
    var _mediaQuery = MediaQuery.of(context);
    return Scaffold(
        body: GestureDetector(
      onTap: () {
        // Navigator.of(context).pushReplacementNamed(UrlPalette.WS2);
      },
      child: Container(
        padding: EdgeInsets.all(0),
        decoration: BoxDecoration(
          color: Theme.of(context).buttonColor,
          borderRadius: BorderRadius.circular(8.0),
        ),
        child: Container(
            height: _mediaQuery.size.height,
            width: _mediaQuery.size.width,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/background.png"),
                fit: BoxFit.fill,
              ),
            ),
            // child: imageCarousel
            child: _isLoading
                ? Center(child: CircularProgressIndicator())
                : Column(children: <Widget>[
                    Padding(padding: EdgeInsets.only(top: 15)),
                    Image.asset(
                      "assets/images/logo_gdi.png",
                      height: 100,
                    ),
                    _bannerOne()
                  ])),
      ),
    ));
  }

  Widget _bannerOne() {
    var _mediaQuery = MediaQuery.of(context);
    // print("height " + _mediaQuery.size.height.toString());
    // print("width " + _mediaQuery.size.width.toString());
    return Padding(
        padding: EdgeInsets.only(
          top: 10,
        ),
        child: Container(
            width: _mediaQuery.size.width - 20,
            margin: EdgeInsets.all(10),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    // Text("asd")
                    Container(
                      // color: Colors.amber,
                      height: _mediaQuery.size.height - 150,
                      width: _mediaQuery.size.width - 20,
                      child: Carousel(
                          boxFit: BoxFit.cover,
                          autoplay: false,
                          animationCurve: Curves.fastOutSlowIn,
                          animationDuration: Duration(milliseconds: 1000),
                          dotSize: 6.0,
                          dotIncreasedColor: Color(0xFFFF335C),
                          dotBgColor: Colors.black12,
                          dotPosition: DotPosition.bottomCenter,
                          dotVerticalPadding: 10.0,
                          showIndicator: true,
                          indicatorBgPadding: 7.0,
                          images: new List.generate(
                              imgList.length,
                              (index) => new Padding(
                                    padding: EdgeInsets.all(0),
                                    child: ListView(
                                      children: <Widget>[
                                        Center(
                                          child: Padding(
                                              padding: EdgeInsets.only(
                                                  top: 0, bottom: 0),
                                              child: new Text(
                                                wordingTopList[index],
                                                style: TextStyle(
                                                    color: Color.fromRGBO(
                                                        255, 97, 0, 1),
                                                    fontSize: 30),
                                              )),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(
                                              top: _paddingTop(_mediaQuery)),
                                          child: Image.asset(
                                            imgList[index],
                                            height: 100,
                                          ),
                                        ),
                                        // Column(
                                        //   mainAxisAlignment: MainAxisAlignment.center,
                                        //   children: <Widget>[
                                        //     Padding(
                                        //         padding: EdgeInsets.only(
                                        //             top: 30, bottom: 20),
                                        //         child: Text(wordingBotList[index]))
                                        //   ],
                                        // ),
                                        Center(
                                            child: Padding(
                                                padding: EdgeInsets.only(
                                                    top: _paddingTop(
                                                        _mediaQuery)),
                                                child: Text(
                                                    wordingBotList[index],
                                                    style: TextStyle(
                                                        fontSize: 18)))),
                                        Center(
                                            child: Padding(
                                                padding:
                                                    EdgeInsets.only(bottom: 0),
                                                child: Text(
                                                    wordingBot2List[index],
                                                    style: TextStyle(
                                                        fontSize: 18)))),
                                        Padding(
                                          padding: EdgeInsets.only(
                                              top: _paddingTop(_mediaQuery)),
                                          child: index == 3
                                              ? Center(
                                                  child: Padding(
                                                      padding: EdgeInsets.only(
                                                          bottom: 0),
                                                      child: Text("Tap disini",
                                                          style: TextStyle(
                                                              fontSize: 18,
                                                              color: Color
                                                                  .fromRGBO(
                                                                      255,
                                                                      97,
                                                                      0,
                                                                      1)))))
                                              : Image.asset(
                                                  'assets/images/garda_digital.png',
                                                  height: 100,
                                                ),
                                        )
                                      ],
                                    ),
                                  )),
                          onImageTap: (item) {
                            if (imgList.length - 1 == item) {
                              Navigator.of(context)
                                  .pushReplacementNamed(UrlPalette.LOGIN);
                            }
                            // print(item);
                          },
                          onImageChange: (a, b) {
                            // print(a);
                            // print(b);
                          }
                          // NetworkImage(
                          //     'https://cdn-images-1.medium.com/max/2000/1*GqdzzfB_BHorv7V2NV7Jgg.jpeg'),
                          // NetworkImage(
                          //     'https://cdn-images-1.medium.com/max/2000/1*wnIEgP1gNMrK5gZU7QS0-A.jpeg'),
                          // ExactAssetImage("assets/images/3.png"),
                          // ],
                          ),
                    ),
                  ],
                ),
              ],
            )));
  }
}

// SizedBox(
//         height: 150.0,
//         width: 300.0,
//         child: Carousel(
//           boxFit: BoxFit.cover,
//           autoplay: false,
//           animationCurve: Curves.fastOutSlowIn,
//           animationDuration: Duration(milliseconds: 1000),
//           dotSize: 6.0,
//           dotIncreasedColor: Color(0xFFFF335C),
//           dotBgColor: Colors.transparent,
//           dotPosition: DotPosition.bottomCenter,
//           dotVerticalPadding: 10.0,
//           showIndicator: true,
//           indicatorBgPadding: 7.0,
//           images: new List.generate(
//               imgList.length,
//               (index) => new Padding(
//                     padding: EdgeInsets.all(0),
//                     child: ListView(
//                       children: <Widget>[
//                         Text(wordingTopList[index]),
//                         Image.asset(
//                           imgList[index],
//                           height: 80,
//                         ),
//                         Text(wordingBotList[index])
//                       ],
//                     ),
//                   )),
//           onImageTap: (item) {
//             print(item);
//           },
//           // onImageChange: (a, b) {
//           //   print(a);
//           //   print(b);
//           // }
//           // NetworkImage(
//           //     'https://cdn-images-1.medium.com/max/2000/1*GqdzzfB_BHorv7V2NV7Jgg.jpeg'),
//           // NetworkImage(
//           //     'https://cdn-images-1.medium.com/max/2000/1*wnIEgP1gNMrK5gZU7QS0-A.jpeg'),
//           // ExactAssetImage("assets/images/3.png"),
//           // ],
//         ),
//       ),
