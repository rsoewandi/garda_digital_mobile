import 'dart:async';
import 'dart:io';

import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:camera/camera.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:garda_digital/global.dart';
import 'package:garda_digital/models/kotaKabupaten.dart';
import 'package:garda_digital/models/provinsi.dart';
import 'package:garda_digital/screens/login_view.dart';
import 'dart:convert';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';

class RegisterPage2 extends StatefulWidget {
  String text;

  RegisterPage2({Key key, this.text}) : super(key: key);

  @override
  _registerState createState() => _registerState();
}

class _registerState extends State<RegisterPage2> {
  String textParameter;
  final _nik = TextEditingController();
  final _nama = TextEditingController();
  final _tanggalLahir = TextEditingController();
  final _kecamatan = TextEditingController();
  final _alamat = TextEditingController();
  final _noWhatsApp = TextEditingController();
  String _selectedDate = 'Tanggal Lahir';
  String _dateBirth;
  String _caution;
  String _valJenisKelamin;
  final _listJenisKelamin = [
    "Laki-Laki",
    "Perempuan",
  ];
  bool showCamera = true;
  CameraController controller;
  bool isReady = false;
  bool _loadingRegister = false;
  String imagePath;

  String timestamp() => DateTime.now().millisecondsSinceEpoch.toString();
  List<CameraDescription> cameras;

  AutoCompleteTextField searchProvinsi;
  AutoCompleteTextField searchKabupaten;
  GlobalKey<AutoCompleteTextFieldState<Provinsi>> keyProvinsi = new GlobalKey();
  GlobalKey<AutoCompleteTextFieldState<KotaKabupaten>> keyKabupaten =
      new GlobalKey();
  static List<Provinsi> listPorvinsi = new List<Provinsi>();
  static List<KotaKabupaten> listKabupaten = new List<KotaKabupaten>();
  bool loading = true;
  bool loadingKab = true;

  void getProvinsi() async {
    try {
      final response = await ApiService.postMapping("/findProvinsi", {});
      if (response.statusCode == 200) {
        listPorvinsi = loadProvinsi(response.body);

        // listKabupaten = loadProvinsi(response.body);
        print('Users: ${listPorvinsi.length}');
        setState(() {
          loading = false;
          // _error = URLS.BASE_URL + " " + response.statusCode.toString();
        });
      } else {
        setState(() {
          // _error = URLS.BASE_URL + " " + response.statusCode.toString();
        });
        print("Error getting users.");
      }
    } catch (e) {
      setState(() {
        // _error = URLS.BASE_URL + " ";
      });
      print("Error getting users.");
    }
  }

  void getKabupaten(Provinsi prov) {
    try {
      setState(() {
        loadingKab = true;
        listKabupaten = prov.kotaKabupaten;
        // loadingKab = false;
      });
      new Timer(new Duration(milliseconds: 30), () {
        debugPrint("Print after 1 seconds");
        setState(() {
          loadingKab = false;
        });
      });
    } catch (e) {
      print("Error getting users.");
    }
  }

  static List<Provinsi> loadProvinsi(String jsonString) {
    final parsed = json.decode(jsonString).cast<Map<String, dynamic>>();
    return parsed.map<Provinsi>((json) => Provinsi.fromJson(json)).toList();
  }

  @override
  void initState() {
    getProvinsi();
    super.initState();
    setupCameras();

    // if (!textParameter.isEmpty) {
    //   Map<String, dynamic> body = jsonDecode(textParameter);
    //   _noWhatsApp.text = body['mobilePhone'];
    // }
  }

  Future<void> setupCameras() async {
    try {
      cameras = await availableCameras();
      controller = new CameraController(cameras[0], ResolutionPreset.medium);
      await controller.initialize();
    } on CameraException catch (_) {
      setState(() {
        isReady = false;
      });
    }
    setState(() {
      isReady = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    this.textParameter = widget.text;
    return Scaffold(
      // backgroundColor: Colors.transparent,
      appBar: new AppBar(
        title: new Text(
          "Register",
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: Color.fromRGBO(255, 97, 0, 1),
        // backgroundColor: Colors.amber[700],
        elevation: 0.0,
      ),
      body: Container(
        padding: EdgeInsets.all(10.0),
        child: ListView(
          children: <Widget>[
            Center(
              child: Column(
                children: <Widget>[
                  _takePicture(),
                  showCamera ? captureControlRowWidget() : Container(),
                  _nikText(),
                  _namaText(),
                  _tglLahirText(),
                  _jenisKelaminDd(),
                  loading
                      // ? CircularProgressIndicator()
                      ? _textLoadingProvinsi()
                      : _textProvinsi(),
                  loadingKab
                      // ? CircularProgressIndicator()
                      ? _textLoadingKabupaten()
                      : _textKabupaten(),
                  _kecamatanText(),
                  _alamatText(),
                  // _textField(context),
                  Padding(padding: EdgeInsets.only(top: 8)),
                  _buttonSave()
                  // FlatButton(
                  //   child: Text(
                  //     'Login',
                  //     style: TextStyle(color: Colors.white),
                  //   ),
                  //   onPressed: () {
                  //     Navigator.of(context)
                  //         .pushReplacementNamed(UrlPalette.LOGIN);
                  //   },
                  // ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _textLoadingProvinsi() {
    return TextFormField(
      decoration: const InputDecoration(
        border: UnderlineInputBorder(),
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: ColorPalette.underLineText,
            width: 1.5,
          ),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: ColorPalette.underLineText,
            width: 3.0,
          ),
        ),
        labelText: "Provinsi",
        labelStyle: TextStyle(color: ColorPalette.textColor),
      ),
      style: TextStyle(color: ColorPalette.textColor),
      autofocus: false,
    );
    ;
  }

  Widget _textLoadingKabupaten() {
    return TextFormField(
      decoration: const InputDecoration(
        border: UnderlineInputBorder(),
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: ColorPalette.underLineText,
            width: 1.5,
          ),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: ColorPalette.underLineText,
            width: 3.0,
          ),
        ),
        labelText: "Kota",
        labelStyle: TextStyle(color: ColorPalette.textColor),
      ),
      style: TextStyle(color: ColorPalette.textColor),
      autofocus: false,
    );
    ;
  }

  Widget _takePicture() {
    return Center(
      child: showCamera
          ? Container(
              // height: 200,
              // width: 250,
              child: Padding(
                padding: const EdgeInsets.only(top: 0),
                child: Center(child: cameraPreviewWidget()),
              ),
            )
          // ],
          // )
          : Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                  imagePreviewWidget(),
                  editCaptureControlRowWidget(),
                ]),
    );
  }

  Widget _nikText() {
    return TextFormField(
      keyboardType: TextInputType.number,
      inputFormatters: [
        LengthLimitingTextInputFormatter(16),
      ],
      controller: _nik,
      decoration: const InputDecoration(
        border: UnderlineInputBorder(),
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: ColorPalette.underLineText,
            width: 1.5,
          ),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: ColorPalette.underLineText,
            width: 3.0,
          ),
        ),
        labelText: "NIK",
        labelStyle: TextStyle(color: ColorPalette.textColor),
      ),
      style: TextStyle(color: ColorPalette.textColor),
      autofocus: false,
    );
  }

  Widget _namaText() {
    return TextFormField(
      controller: _nama,
      inputFormatters: [
        LengthLimitingTextInputFormatter(200),
      ],
      decoration: const InputDecoration(
        border: UnderlineInputBorder(),
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: ColorPalette.underLineText,
            width: 1.5,
          ),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: ColorPalette.underLineText,
            width: 3.0,
          ),
        ),
        labelText: "Nama",
        labelStyle: TextStyle(color: ColorPalette.textColor),
      ),
      style: TextStyle(color: ColorPalette.textColor),
      autofocus: false,
    );
  }

  Widget _tglLahirText() {
    return new Row(children: <Widget>[
      new Expanded(
          child: new TextFormField(
              controller: _tanggalLahir,
              showCursor: true,
              readOnly: true,
              onTap: () {
                _selectDate(context);
              },
              style: TextStyle(color: ColorPalette.textColor),
              decoration: const InputDecoration(
                labelText: "Tanggal Lahir",
                labelStyle: TextStyle(color: ColorPalette.textColor),
                // border: UnderlineInputBorder(),
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(
                    color: ColorPalette.underLineText,
                    width: 1.5,
                  ),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(
                    color: ColorPalette.underLineText,
                    width: 3.0,
                  ),
                ),
              ))),
      Padding(
          padding: EdgeInsets.only(top: 10),
          child: IconButton(
            icon: Icon(
              Icons.calendar_today,
              color: ColorPalette.iconText,
            ),
            tooltip: 'Tanggal Lahir',
            onPressed: () {
              _selectDate(context);
            },
          )),
    ]);
  }

  Widget _jenisKelaminDd() {
    return Theme(
        data: new ThemeData(
            canvasColor: Color.fromRGBO(255, 97, 0, 1),
            primaryColor: Colors.white,
            accentColor: Colors.white,
            hintColor: Colors.white),
        child: DropdownButtonFormField<String>(
          //isDense: true,
          decoration: const InputDecoration(
              border: UnderlineInputBorder(),
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(
                  color: ColorPalette.underLineText,
                  width: 1.5,
                ),
              )),
          hint: Text('Jenis Kelamin',
              style: TextStyle(color: ColorPalette.textColor)),
          value: _valJenisKelamin,
          icon: Icon(Icons.check_circle_outline, color: ColorPalette.textColor),
          iconSize: 24,
          elevation: 26,
          style: TextStyle(color: ColorPalette.textColor),
          onChanged: (String newValue) {
            setState(() {
              _valJenisKelamin = newValue;
            });
          },
          items: _listJenisKelamin.map((value) {
            return DropdownMenuItem(
              child: Text(value),
              value: value,
            );
          }).toList(),
        ));
  }

  Widget _textProvinsi() {
    return searchProvinsi = AutoCompleteTextField<Provinsi>(
        key: keyProvinsi,
        clearOnSubmit: false,
        submitOnSuggestionTap: true,
        suggestions: listPorvinsi,
        style: TextStyle(color: Colors.black, fontSize: 16.0),
        decoration: InputDecoration(
          // contentPadding: EdgeInsets.fromLTRB(10.0, 30.0, 10.0, 20.0),
          hintText: "Provinsi",
          hintStyle: TextStyle(color: Colors.black),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: ColorPalette.underLineText,
              width: 1.5,
            ),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: ColorPalette.underLineText,
              width: 3.0,
            ),
          ),
        ),
        itemFilter: (item, query) {
          return item.namaProvinsi.toLowerCase().contains(query.toLowerCase());
        },
        itemSorter: (a, b) {
          return a.namaProvinsi.compareTo(b.namaProvinsi);
        },
        itemSubmitted: (item) {
          setState(() {
            searchProvinsi.textField.controller.text = item.namaProvinsi;
            getKabupaten(item);
          });
        },
        itemBuilder: (context, item) {
          // ui for the autocompelete row
          return row(item);
        });
  }

  Widget _textKabupaten() {
    return searchKabupaten = new AutoCompleteTextField<KotaKabupaten>(
        key: keyKabupaten,
        clearOnSubmit: false,
        suggestions: listKabupaten,
        style: TextStyle(color: Colors.black, fontSize: 16.0),
        decoration: InputDecoration(
          // contentPadding: EdgeInsets.fromLTRB(10.0, 30.0, 10.0, 20.0),
          hintText: "Kota",
          hintStyle: TextStyle(color: Colors.black),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: ColorPalette.underLineText,
              width: 1.5,
            ),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              color: ColorPalette.underLineText,
              width: 3.0,
            ),
          ),
        ),
        itemFilter: (item, query) {
          return item.namaKotaKabupaten
              .toLowerCase()
              .contains(query.toLowerCase());
        },
        itemSorter: (a, b) {
          return a.namaKotaKabupaten.compareTo(b.namaKotaKabupaten);
        },
        itemSubmitted: (item) {
          setState(() {
            searchKabupaten.textField.controller.text = item.namaKotaKabupaten;
          });
        },
        itemBuilder: (context, item) {
          // ui for the autocompelete row
          return rowKabupaten(item);
        });
  }

  Widget rowKabupaten(KotaKabupaten kab) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Padding(
            padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
            child:
                Text(kab.namaKotaKabupaten, style: TextStyle(fontSize: 16.0))),
        // Text(
        //   user,
        //   style: TextStyle(fontSize: 16.0, height: 2.5),
        // ),
        SizedBox(
          width: 15.0,
        ),
        // Text(
        //   user.email,
        // ),
      ],
    );
  }

  Widget _kecamatanText() {
    return TextFormField(
      controller: _kecamatan,
      decoration: const InputDecoration(
        border: UnderlineInputBorder(),
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: ColorPalette.underLineText,
            width: 1.5,
          ),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: ColorPalette.underLineText,
            width: 3.0,
          ),
        ),
        labelText: "Kecamatan",
        labelStyle: TextStyle(color: ColorPalette.textColor),
      ),
      style: TextStyle(color: ColorPalette.textColor),
      autofocus: false,
    );
  }

  Widget _alamatText() {
    return TextFormField(
      controller: _alamat,
      decoration: const InputDecoration(
        border: UnderlineInputBorder(),
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: ColorPalette.underLineText,
            width: 1.5,
          ),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: ColorPalette.underLineText,
            width: 3.0,
          ),
        ),
        labelText: "Alamat Tempat Tinggal",
        labelStyle: TextStyle(color: ColorPalette.textColor),
      ),
      style: TextStyle(color: ColorPalette.textColor),
      autofocus: false,
    );
  }

  Widget _buttonSave() {
    return Container(
        padding: EdgeInsets.symmetric(vertical: 8.0),
        width: double.infinity,
        child: InkWell(
          onTap: () {
            if (_loadingRegister == true) {
              return;
            }
            var idProvinsi;
            var idKabupaten;
            for (Provinsi prov in listPorvinsi) {
              if (searchProvinsi.textField.controller.text
                  .contains(prov.namaProvinsi)) {
                idProvinsi = prov.namaProvinsi;

                for (KotaKabupaten kota in prov.kotaKabupaten) {
                  if (searchKabupaten.textField.controller.text
                      .contains(kota.namaKotaKabupaten)) {
                    idKabupaten = kota.namaKotaKabupaten;
                  }
                }
                break;
              }
            }
            print('clicked');

            var _error = [];
            if (imagePath == null) {
              _error.add("Photo KTP");
            }
            // if (_nik.text == "") {
            // }

            if (_nik.text.length < 16) {
              _error.add("NIK harus 16 Angka");
            }
            if (_nama.text == "") {
              _error.add("Nama");
            }
            if (_valJenisKelamin == null) {
              _error.add("Jenis Kelamin");
            }
            if (_dateBirth == null) {
              _error.add("Tanggal Lahir");
            }
            if (idProvinsi == null) {
              _error.add("Provinsi");
            }
            if (idKabupaten == null) {
              _error.add("Kota");
            }
            if (_kecamatan.text == "") {
              _error.add("Kecamatan");
            }
            if (_alamat.text == "") {
              _error.add("Alamat Tempat Tinggal");
            }

            if (_error.length > 0) {
              _loadingRegister = false;
              _caution = "Mohon isi \n- ";
              int num = 1;
              for (var obj in _error) {
                if (_error.length == num) {
                  _caution += obj;
                  //"dan " + obj;
                } else {
                  _caution += obj + " \n- ";
                }
                num++;
              }
              showAlertDialog(context);
              return;
            }

            // if (_valJenisKelamin == null) {
            //   _loadingRegister = false;
            //   _caution = "Mohon isi Jenis Kelamin ";
            //   showAlertDialog(context);
            //   return;
            // }
            final bytes = File(imagePath).readAsBytesSync();
            var base64Image = base64Encode(bytes);
            // String body = jsonEncode({
            //   "mobilePhone": _noWhatsApp.text,
            //   "komunitas": _komunitas.text,
            //   "statusPerkawinan": _valStatusPerkawinan,
            //   "jumlahAnak": _jumlahAnak.text,
            //   "jenisKendaraan": _valJenisKendaraan,
            //   "merkKendaraan": _merkKendaraan.text,
            //   "tipeKendaraan": _tipeKendaraan.text,
            //   "tahunPembuatan": _tahunPembuatan.text,
            //   "noPol": _noPol.text,
            // });
            if (!textParameter.isEmpty) {
              Map<String, dynamic> jsonParam = jsonDecode(textParameter);
              var mobilePhone = jsonParam['mobilePhone'];

              var gender;
              if (jsonParam['statusPerkawinan'].contains("Belum Menikah")) {
                gender = "BM";
              }
              if (jsonParam['statusPerkawinan'].contains("Menikah")) {
                gender = "M";
              }
              if (jsonParam['statusPerkawinan'].contains("Berpisah")) {
                gender = "B";
              }
              if (_valJenisKelamin != null) {
                if (_valJenisKelamin.contains("Laki-Laki")) {
                  gender = "L";
                }
                if (_valJenisKelamin.contains("Perempuan")) {
                  gender = "P";
                }
              } else {
                gender = "";
              }

              print(_dateBirth);

              final body = {
                "alamat": _alamat.text,
                "anggotaKomunitas": jsonParam['komunitas'],
                "birthDate": _dateBirth,
                "fullName": _nama.text,
                "gender": gender,
                "idNumber": _nik.text,
                "idOjol": "string",
                "jenisKendaraan": jsonParam['jenisKendaraan'],
                "jumlahAnak": jsonParam['jumlahAnak'],
                "kecamatan": _kecamatan.text,
                "kota": idKabupaten,
                "merkKendaraan": jsonParam['merkKendaraan'],
                "mobilePhone": mobilePhone,
                "nopol": jsonParam['noPol'],
                "provinsi": idProvinsi,
                "statusPerkawinan": jsonParam['statusPerkawinan'],
                "tahunPembuatan": jsonParam['tahunPembuatan'],
                "tipeKendaraan": jsonParam['tipeKendaraan'],
                "userPicture": {
                  // "avatarUser": "string",
                  // "id": 0,
                  "ktpImage": base64Image
                }
              };

              _loadingRegister = true;
              showLoadingBar(context);
              ApiService.postMapping("/register", body).then((success) {
                Navigator.pop(context);
                _loadingRegister = false;
                Map<String, dynamic> body = jsonDecode(success.body);
                if (body['status'] == 200) {
                  Widget okButton = FlatButton(
                    child: Text("OK"),
                    onPressed: () => Navigator.of(context)
                        .pushReplacementNamed(UrlPalette.LOGIN),
                  );
                  showDialog(
                      context: context,
                      builder: (context) {
                        return AlertDialog(
                          title: Text("Caution"),
                          content: Text("Registrasi Berhasil"),
                          actions: <Widget>[okButton],
                        );
                      });
                } else {
                  _caution = body['errorMessage'];
                  showAlertDialog(context);
                }
              });
            }

            // Navigator.of(context).pushReplacementNamed(UrlPalette.LOGIN);
          },
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 8.0),
            width: double.infinity,
            child: Text(
              'Daftar',
              style: TextStyle(color: Colors.white, fontSize: 20),
              textAlign: TextAlign.center,
            ),
            decoration: BoxDecoration(
              color: Color.fromRGBO(255, 97, 0, 1),
              borderRadius: BorderRadius.circular(30.0),
            ),
          ),
        ));
  }

  showAlertDialog(BuildContext context) {
    // set up the button
    Widget okButton = FlatButton(
      child: Text("OK"),
      onPressed: () => Navigator.pop(context),
    );
    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      // title: Text("Caution"),
      content: Text(_caution),
      actions: [
        okButton,
      ],
    );
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  successAlert(BuildContext context) {
    // set up the button
    Widget okButton = FlatButton(
        child: Text("OK"),
        // onPressed: () => Navigator.pop(context),
        onPressed: () =>
            Navigator.of(context).pushReplacementNamed(UrlPalette.LOGIN));
    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Caution"),
      content: Text("Data berhasil disimpan"),
      actions: [
        okButton,
      ],
    );
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  showLoadingBar(BuildContext context) {
    // set up the button
    // Widget okButton = FlatButton(
    //   child: Text("OK"),
    //   onPressed: () => Navigator.pop(context),
    //   // onPressed: () => Navigator.pop(context),
    // );
    // set up the AlertDialog
    if (_loadingRegister == true) {
      AlertDialog alert = AlertDialog(
        backgroundColor: Colors.transparent,

        // title: Text("Caution"),
        // content: Padding(
        //     padding: EdgeInsets.only(left: 10),
        //     child: CircularProgressIndicator()),
        content: Container(
            padding: EdgeInsets.only(left: 10),
            margin: EdgeInsets.only(right: 100, left: 100),
            height: 20,
            width: 20,
            child: CircularProgressIndicator(
              backgroundColor: Colors.transparent,
            )),
        // actions: [
        //   okButton,
        // ],
      );
      // show the dialog
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return alert;
        },
      );
    }
  }

  Widget _textField(BuildContext context) {
    return Column(
      children: <Widget>[
        // Padding(
        //   padding: EdgeInsets.only(top: 12.0),
        // ),

        // Padding(
        //   padding: EdgeInsets.only(top: 12.0),
        // ),
      ],
    );
  }

  Future<void> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: DateTime(1970, 1),
        firstDate: DateTime(1970, 8),
        lastDate: DateTime(2001));
    setState(() {
      _selectedDate = new DateFormat.yMd("en_US").format(picked);
      _dateBirth = DateFormat('yyyy-MM-dd').format(picked);
      _tanggalLahir.text = new DateFormat.yMd("en_US").format(picked);
    });
  }

  Widget row(Provinsi user) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Padding(
            padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
            child: Text(user.namaProvinsi, style: TextStyle(fontSize: 16.0))),
        // Text(
        //   user,
        //   style: TextStyle(fontSize: 16.0, height: 2.5),
        // ),
        SizedBox(
          width: 15.0,
        ),
      ],
    );
  }

  Widget cameraPreviewWidget() {
    if (!isReady || !controller.value.isInitialized) {
      return Container();
    }
    final size = MediaQuery.of(context).size;
    return Transform.scale(
      scale: 1.0,
      child: AspectRatio(
        aspectRatio: 3.0 / 2.0,
        child: OverflowBox(
          alignment: Alignment.center,
          child: FittedBox(
            fit: BoxFit.fitWidth,
            child: Container(
              width: size.aspectRatio,
              height: size.aspectRatio / controller.value.aspectRatio,
              child: Stack(
                children: <Widget>[
                  CameraPreview(controller),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget imagePreviewWidget() {
    final size = MediaQuery.of(context).size;
    return Transform.scale(
      scale: 1.0,
      child: AspectRatio(
        aspectRatio: 3.0 / 2.0,
        child: OverflowBox(
          alignment: Alignment.center,
          child: FittedBox(
            fit: BoxFit.fitWidth,
            child: Container(
              width: size.aspectRatio,
              height: size.aspectRatio / controller.value.aspectRatio,
              child: Stack(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 0),
                    child: Image.file(File(imagePath)),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget editCaptureControlRowWidget() {
    return Padding(
      padding: const EdgeInsets.only(top: 5),
      child: Align(
        alignment: Alignment.topCenter,
        child:
            // IconButton(
            //   icon: const Icon(Icons.camera_alt),
            //   color: ColorPalette.iconText,
            //   onPressed: () => setState(() {
            //     showCamera = true;
            //   }),
            // ),
            InkWell(
          onTap: () => setState(() {
            showCamera = true;
          }),
          child: Container(
            child: Padding(
                padding: EdgeInsets.all(10),
                child: Text(
                  'Edit Foto',
                  style: TextStyle(color: ColorPalette.white),
                  textAlign: TextAlign.center,
                )),
            decoration: BoxDecoration(
              color: Color.fromRGBO(255, 97, 0, 1),
              borderRadius: BorderRadius.circular(2),
            ),
          ),
        ),
      ),
    );
  }

  Widget captureControlRowWidget() {
    return new Padding(
        padding: EdgeInsets.only(top: 10),
        child: new Row(children: <Widget>[
          new Expanded(
              child: new Container(
            padding: EdgeInsets.only(top: 12),
            decoration: BoxDecoration(
                border: Border(
              bottom: BorderSide(width: 1.5, color: ColorPalette.underLineText),
            )),
            child: Text("Foto Ktp", style: TextStyle(fontSize: 16)),
          )
              // new TextFormField(
              //     showCursor: true,
              //     readOnly: true,
              //     style: TextStyle(color: ColorPalette.textColor),
              //     decoration: const InputDecoration(
              //       hintText: "Foto Ktp",
              //       hintStyle: TextStyle(color: ColorPalette.textColor),
              //       // border: UnderlineInputBorder(),
              //       enabledBorder: UnderlineInputBorder(
              //         borderSide: BorderSide(
              //           color: ColorPalette.underLineText,
              //           width: 1.5,
              //         ),
              //       ),
              //       focusedBorder: UnderlineInputBorder(
              //         borderSide: BorderSide(
              //           color: ColorPalette.underLineText,
              //           width: 3.0,
              //         ),
              //       ),

              ),

          // alignment: Alignment.topCenter,
          InkWell(
            onTap: controller != null && controller.value.isInitialized
                ? onTakePictureButtonPressed
                : null,
            child: Container(
              child: Padding(
                  padding: EdgeInsets.all(10),
                  child: Text(
                    'Ambil Foto',
                    style: TextStyle(color: ColorPalette.white),
                    textAlign: TextAlign.center,
                  )),
              decoration: BoxDecoration(
                color: Color.fromRGBO(255, 97, 0, 1),
                borderRadius: BorderRadius.circular(2),
              ),
            ),
          )
        ]));
  }

  void onTakePictureButtonPressed() {
    takePicture().then((String filePath) {
      if (mounted) {
        setState(() {
          showCamera = false;
          imagePath = filePath;
        });
      }
    });
  }

  Future<String> takePicture() async {
    if (!controller.value.isInitialized) {
      return null;
    }
    final Directory extDir = await getApplicationDocumentsDirectory();
    final String dirPath = '${extDir.path}/Pictures/flutter_test';
    await Directory(dirPath).create(recursive: true);
    final String filePath = '$dirPath/${timestamp()}.jpg';

    if (controller.value.isTakingPicture) {
      return null;
    }

    try {
      await controller.takePicture(filePath);
    } on CameraException catch (e) {
      return null;
    }
    return filePath;
  }

  // List<Provinsi> findProvinsi() {
  //   ApiService.findProvinsi().then((success) {
  //     return success;
  //   });
  // }
}
