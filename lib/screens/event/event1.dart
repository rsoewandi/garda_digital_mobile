import 'dart:async';
import 'dart:convert';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:garda_digital/global.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:intl/intl.dart';

class EventOnePage extends StatefulWidget {
  String text;

  EventOnePage({Key key, this.text}) : super(key: key);
  @override
  State createState() => new MainWidgetState();
}

class MainWidgetState extends State<EventOnePage> {
  var _parameter;
  String _textDetail =
      "Detail/Deskripsi Event. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dapibus ultrices in iaculis nunc sed augue lacus. Quam nulla porttitor massa id neque aliquam. Ultrices mi tempus imperdiet nulla malesuada. Eros in cursus turpis massa tincidunt dui ut ornare lectus. Egestas sed sed risus pretium. Lorem dolor sed viverra ipsum. Gravida rutrum quisque non tellus. Rutrum tellus pellentesque eu tincidunt tortor. Sed blandit libero volutpat sed cras ornare. Et netus et malesuada fames ac. Ultrices eros in cursus turpis massa tincidunt dui ut ornare. Lacus sed viverra tellus in. Sollicitudin ac orci phasellus egestas. Purus in mollis nunc sed. Sollicitudin ac orci phasellus egestas tellus rutrum tellus pellentesque. Interdum consectetur libero id faucibus nisl tincidunt eget.";
  String _longText;
  String _textlihatSemua;
  bool _isLoading = true;
  num _sizeText = 137;
  String dateStart;

  _wrapText() async {
    _parameter = jsonDecode(widget.text);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var token = "Bearer " + prefs.get(SessionString.token).toString();
    DateTimeIndo.getDate(new DateFormat('dd-M-yyyy').format(
            DateTime.fromMicrosecondsSinceEpoch(
                _parameter['dateStart'] * 1000)))
        .then((value) {
      setState(() {
        dateStart = value;
      });
    });
    final bodyReq = {"GSocialID": _parameter['id']};
    ApiService.postMappingWithToken("/gsocial/findRemainQuota", bodyReq, token)
        .then((success) {
      setState(() {
        _isLoading = false;
      });
      if (success.statusCode == 200) {
        _parameter['quota'] = jsonDecode(success.body);
        if (_parameter['description'].toString().length < 137) {
          _longText = _parameter['description'];
          setState(() {
            _textlihatSemua = "";
            _isLoading = false;
          });
        } else {
          _textDetail = _parameter['description'];
          var str = _textDetail.split("");
          _longText = "";
          num x = 0;
          for (var item in str) {
            _longText += item;
            x++;
            if (x == _sizeText) {
              setState(() {
                _textlihatSemua = " Lihat Semua";
                _isLoading = false;
              });
              break;
            }
          }
        }
      } else {
        prefs.clear();
        Navigator.of(context).pushReplacementNamed(UrlPalette.LOGIN);
      }
    });
  }

  _registrationGSosial() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var token = "Bearer " + prefs.get(SessionString.token).toString();
    final bodyReq = {
      "GSocialID": _parameter['id'],
      "mobilePhone": prefs.get(SessionString.noTelp)
    };

    ApiService.postMappingWithToken("/gsocial/registration", bodyReq, token)
        .then((success) {
      setState(() {
        _isLoading = false;
      });
      if (success.statusCode == 200) {
        Map<String, dynamic> body = jsonDecode(success.body);
        Widget okButton = FlatButton(
          child: Text("OK"),
          onPressed: () {
            if (body['status'] == 200) {
              Navigator.of(context).pushNamedAndRemoveUntil(
                  UrlPalette.DASHBOARD, (Route<dynamic> route) => false);
            } else {
              Navigator.pop(context);
            }
          },
        );
        showDialog(
            context: context,
            builder: (context) {
              return AlertDialog(
                content: Container(
                    // margin: EdgeInsets.only(top: 50),
                    child: Text(body['message'])),
                actions: <Widget>[okButton],
              );
            });
      } else {
        prefs.clear();
        Navigator.of(context).pushReplacementNamed(UrlPalette.LOGIN);
      }
    });
  }

  @override
  void initState() {
    _wrapText();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var _mediaQuery = MediaQuery.of(context);
    return Scaffold(
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(30.0), // here the desired height
          child: new AppBar(
            leading: IconButton(
              padding: EdgeInsets.only(top: 1, bottom: 2, right: 25),
              onPressed: () async {
                var nav = await Navigator.of(context)
                    .pushReplacementNamed(UrlPalette.SOSIALPAGE);

                // Navigator.of(context).pushNamedAndRemoveUntil(
                //     UrlPalette.SOSIALPAGE, (Route<dynamic> route) => false);
                if (nav == null) {
                  //change the state

                  Navigator.of(context).pushNamedAndRemoveUntil(
                      UrlPalette.DASHBOARD, (Route<dynamic> route) => false);
                  // nav = await Navigator.of(context)
                  //     .pushReplacementNamed(UrlPalette.DASHBOARD);
                }
                // Navigator.pop(context);
                // Navigator.of(context)
                //     .pushReplacementNamed(UrlPalette.SOSIALPAGE);
              },
              icon: Icon(
                Icons.chevron_left,
                size: 23,
                color: Colors.white,
              ), // change this size an
            ),
            title: Container(
                margin: EdgeInsets.only(right: 50),
                child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      new Text(
                        "G-Social",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.bold),
                      )
                    ])),
            backgroundColor: Color.fromRGBO(255, 97, 0, 1),
            elevation: 0.0,
          )),
      body: Container(
        child: _loadingScreen()
            ? Center(child: CircularProgressIndicator())
            // : Column(
            //     mainAxisAlignment: MainAxisAlignment.center,
            //     mainAxisSize: MainAxisSize.max,
            //     crossAxisAlignment: CrossAxisAlignment.center,
            //     children: <Widget>[
            //         Text("aa"),
            //         Text("bb"),
            //       ])
            : ListView(
                children: <Widget>[
                  _headerBanner(_mediaQuery),
                  _titleOne(),
                  _dates(),
                  _city(),
                  _quota(),
                  _detail(),
                  _lokasi(),
                  _sk(),
                  _buttonRegis(_mediaQuery)
                ],
              ),
      ),
    );
  }

  _loadingScreen() {
    if (_isLoading) {
      return true;
    }
    // if (_isLoadingSignIn) {
    //   return true;
    // }
    return false;
  }

  Widget _headerBanner(var _mediaQuery) {
    return Row(
      children: <Widget>[
        SizedBox(
          width: _mediaQuery.size.width,
          child: Image.network(_parameter['imageBannerName']),
          // child: Image.asset("assets/images/ads4.png"),
        ),
      ],
    );
  }

  Widget _titleOne() {
    return Container(
        padding: EdgeInsets.only(top: 10, left: 10, right: 10),
        child: Container(
          padding: EdgeInsets.only(bottom: 10),
          decoration: BoxDecoration(
              border: Border(
            bottom: BorderSide(width: 1.0, color: Colors.black12),
          )),
          child: Row(
            children: <Widget>[
              Text(_parameter['title'].toString(), //"Event Title G-Social",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16))
            ],
          ),
        ));
  }

  Widget _dates() {
    return Container(
        padding: EdgeInsets.only(top: 3, left: 10, right: 10),
        child: Container(
          padding: EdgeInsets.only(bottom: 3),
          decoration: BoxDecoration(
              border: Border(
            bottom: BorderSide(width: 1.0, color: Colors.black12),
          )),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[Text("Tanggal"), Text(dateStart)],
          ),
        ));
  }

  Widget _city() {
    return Container(
        padding: EdgeInsets.only(top: 3, left: 10, right: 10),
        child: Container(
          padding: EdgeInsets.only(bottom: 3),
          decoration: BoxDecoration(
              border: Border(
            bottom: BorderSide(width: 1.0, color: Colors.black12),
          )),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text("Kota"),
              Text(_parameter['city'])
              // Text("Bandung")
            ],
          ),
        ));
  }

  Widget _quota() {
    return Container(
        padding: EdgeInsets.only(top: 3, left: 10, right: 10),
        child: Container(
          padding: EdgeInsets.only(bottom: 3),
          decoration: BoxDecoration(
              border: Border(
            bottom: BorderSide(width: 1.0, color: Colors.black12),
          )),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text("Sisa Quota"),

              Text(_parameter['quota'].toString() + " Peserta")
              // Text("163 Peserta")
            ],
          ),
        ));
  }

  Widget _detail() {
    return Container(
        padding: EdgeInsets.only(top: 3, left: 10, right: 10),
        child: Container(
            padding: EdgeInsets.only(bottom: 3),
            decoration: BoxDecoration(
                border: Border(
              bottom: BorderSide(width: 1.0, color: Colors.black12),
            )),
            child: Wrap(
              // alignment: WrapAlignment.,

              children: <Widget>[
                new RichText(
                  textAlign: TextAlign.justify,
                  text: new TextSpan(
                    // Note: Styles for TextSpans must be explicitly defined.
                    // Child text spans will inherit styles from parent
                    style: new TextStyle(
                      color: Colors.black,
                    ),
                    children: <TextSpan>[
                      new TextSpan(
                          style: new TextStyle(fontFamily: 'Montserrat'),
                          text: _longText),
                      new TextSpan(
                        text: _textlihatSemua,
                        style: new TextStyle(
                            fontFamily: 'Montserrat', color: Colors.red),
                        recognizer: new TapGestureRecognizer()
                          ..onTap = () {
                            setState(() {
                              if (_longText.length == _sizeText) {
                                _textlihatSemua = ' Kecilkan';
                                _longText = _textDetail;
                              } else {
                                _wrapText();
                              }
                            });
                          },
                      ),
                    ],
                  ),
                )
              ],
            )));
  }

  Widget _lokasi() {
    return Container(
        padding: EdgeInsets.only(top: 3, left: 10, right: 10),
        child: Container(
          padding: EdgeInsets.only(bottom: 3),
          decoration: BoxDecoration(
              border: Border(
            bottom: BorderSide(width: 1.0, color: Colors.black12),
          )),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text("Lokasi G-Social"),

              Text(_parameter['location']),
              // Text("Halaman gedung gede, Bandung"),
              Text("")
            ],
          ),
        ));
  }

  Widget _sk() {
    return Container(
        padding: EdgeInsets.only(top: 3, left: 10, right: 10),
        child: Container(
          padding: EdgeInsets.only(bottom: 3),
          decoration: BoxDecoration(
              border: Border(
            bottom: BorderSide(width: 1.0, color: Colors.black12),
          )),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                "S&K",
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              Text(""),

              Text(_parameter['termsCondition'], textAlign: TextAlign.justify),
              // Text("SPESIAL GRATIS LAGI"),
              // Text(
              //   "1. Terdaftar sebagai member GARDA DIGITAL",
              // ),
              // Text(
              //   "2. Belum pernah mengikuti Event G-Social selama 3 bulan terakhir.",
              //   textAlign: TextAlign.justify,
              // ),
              // Text(
              //   "3. Hanya berlaku bagi member di kota temapt event ini diadakan.",
              //   textAlign: TextAlign.justify,
              // ),
              // Text(
              //   "4. Sudah mendaftarkan diri untuk Event ini.",
              //   textAlign: TextAlign.justify,
              // ),
              // Text(
              //   "5. Selama sisa quota peserta masih tersedia.",
              //   textAlign: TextAlign.justify,
              // ),
            ],
          ),
        ));
  }

  Widget _buttonRegis(var _mediaQuery) {
    return Container(
      padding: EdgeInsets.only(
          top: 13,
          left: _mediaQuery.size.width / 3.2,
          right: _mediaQuery.size.width / 3.2,
          bottom: 10),
      child: Container(
        padding: EdgeInsets.only(bottom: 3),
        // decoration: BoxDecoration(
        //     border: Border(
        //   bottom: BorderSide(width: 1.0, color: Colors.black12),
        // )),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  color: Colors.white,
                  boxShadow: [
                    new BoxShadow(
                      color: Colors.black26,
                      offset: const Offset(7.0, 7.0),
                      blurRadius: 8.0,
                    ),
                  ],
                ),
                child: InkWell(
                  onTap: () {
                    _registrationGSosial();
                  },
                  child: Container(
                    padding:
                        EdgeInsets.only(left: 0.0, top: 10.0, bottom: 10.0),
                    width: double.infinity,
                    child: Text(
                      'DAFTARKAN DIRI',
                      style: TextStyle(color: Colors.white, fontSize: 14.0),
                      textAlign: TextAlign.center,
                    ),
                    decoration: BoxDecoration(
                      color: ColorPalette.orange,
                      borderRadius: BorderRadius.circular(5),
                    ),
                  ),
                ))
          ],
        ),
      ),
    );
  }
}
