import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:garda_digital/global.dart';
import 'package:garda_digital/screens/dashboard.dart';
import 'package:garda_digital/screens/otp_view.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginPage extends StatefulWidget {
  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<LoginPage> {
  bool _isLoading = true;
  bool _isLoadingSignIn = false;
  final FocusNode _ageFocus = FocusNode();
  final FocusNode _heightFocus = FocusNode();
  String _caution;
  String tokenFcm;
  final _noWhatsApp = TextEditingController();

  @override
  void initState() {
    // disable fcm
    // new Timer(new Duration(seconds: 1), () {
    //   firebase().firebaseNotification(context);
    // });
    super.initState();
  }

  signIn(String email, pass) async {
    // showLoading(context);
    setState(() {
      _isLoadingSignIn = true;
    });
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    tokenFcm = sharedPreferences.get(SessionString.fcmToken);

    final mapBody = {"mobilePhone": _noWhatsApp.text, "fcmToken": tokenFcm};
    var response = await ApiService.postMapping("/generateOTP", mapBody);
    Map<String, dynamic> body = jsonDecode(response.body);
    if (body['status'] == 200) {
      sharedPreferences.setString(SessionString.otp, body['otpCode']);
      sharedPreferences.setString(SessionString.noTelp, _noWhatsApp.text);

      new Timer(new Duration(seconds: 1), () {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (BuildContext context) => OtpPage()),
            (Route<dynamic> route) => false);
      });
    } else {
      setState(() {
        _isLoadingSignIn = false;
      });
      _caution = body['errorMessage'];
      showAlertDialog(context, "Nomor Ponsel tidak terdaftar");
    }
  }

  @override
  Widget build(BuildContext context) {
    new Timer(new Duration(seconds: 1), () {
      SessionLogin.cek().then((_cekSession) {
        if (_cekSession == true) {
          Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(
                  builder: (BuildContext context) => DashboardPage()),
              (Route<dynamic> route) => false);
        } else {
          setState(() {
            _isLoading = false;
          });
        }
      });
    });
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/background.png"),
            fit: BoxFit.fill,
          ),
        ),
        padding: EdgeInsets.all(20.0),
        child: _loadingScreen()
            ? Center(child: CircularProgressIndicator())
            // : Column(
            //     mainAxisAlignment: MainAxisAlignment.center,
            //     mainAxisSize: MainAxisSize.max,
            //     crossAxisAlignment: CrossAxisAlignment.center,
            //     children: <Widget>[
            //         Text("aa"),
            //         Text("bb"),
            //       ])
            : ListView(
                children: <Widget>[
                  Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      mainAxisSize: MainAxisSize.max,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        _titleDescription(),
                        _iconLogin(),
                        _textField(context),
                      ],
                    ),
                  ),
                ],
              ),
      ),
    );
  }

  _loadingScreen() {
    if (_isLoading) {
      return true;
    }
    if (_isLoadingSignIn) {
      return true;
    }
    return false;
  }

  Widget _iconLogin() {
    return Image.asset(
      "assets/images/logo_gdi.png",
      width: 200.0,
      height: 200.0,
    );
  }

  Widget _titleDescription() {
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(bottom: 15.0),
        ),
      ],
    );
  }

  _textPosition(var _mediaQuery) {
    var result;
    if (_mediaQuery.size.height < 650) {
      result = _mediaQuery.size.height / 5;
    } else if (_mediaQuery.size.height > 650 && _mediaQuery.size.height < 860) {
      result = _mediaQuery.size.height / 2.5;
    } else {
      result = _mediaQuery.size.height / 2.3;
    }
    return result;
  }

  Widget _textField(BuildContext context) {
    var _mediaQuery = MediaQuery.of(context);
    // print(_mediaQuery.size);
    return Container(
        // color: Colors.white,
        padding: EdgeInsets.only(top: _textPosition(_mediaQuery)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            // Padding(
            //   padding: EdgeInsets.only(top: 85),
            // ),
            new Row(children: <Widget>[
              Padding(
                  padding: EdgeInsets.only(top: 0, right: 5),
                  child: Icon(
                    Icons.account_circle,
                    size: 40,
                    color: ColorPalette.white,
                  )),
              new Expanded(
                  child: new TextFormField(
                controller: _noWhatsApp,
                focusNode: _ageFocus,
                onFieldSubmitted: (term) {
                  _fieldFocusChange(context, _ageFocus, _heightFocus);
                },
                keyboardType: TextInputType.number,
                decoration: new InputDecoration(
                  hintText: "Nomor Ponsel",
                  hintStyle: TextStyle(color: ColorPalette.hintColor),
                  fillColor: Colors.white,
                  focusedBorder: UnderlineInputBorder(
                      borderRadius: BorderRadius.circular(0),
                      borderSide: BorderSide(
                        color: Colors.white,
                        width: 2.0,
                      )),
                  enabledBorder: UnderlineInputBorder(
                      borderRadius: BorderRadius.circular(0),
                      borderSide: BorderSide(
                        color: Colors.white,
                        width: 2.0,
                      )),
                ),

                // decoration: const InputDecoration(
                //   border: UnderlineInputBorder(),
                //   enabledBorder: UnderlineInputBorder(
                //     borderSide: BorderSide(
                //       color: ColorPalette.underLineText,
                //       width: 1.5,
                //     ),
                //   ),
                //   focusedBorder: UnderlineInputBorder(
                //     borderSide: BorderSide(
                //       color: ColorPalette.underLineText,
                //       width: 3.0,
                //     ),
                //   ),
                //   labelText: "No.Pol STNK",
                //   labelStyle: TextStyle(color: ColorPalette.textColor),
                // ),
                style: TextStyle(color: Colors.white),
              ))
            ]),
            Padding(
              padding: EdgeInsets.only(top: 12.0),
            ),

            _buildButton(context),
          ],
        ));
  }

  Widget _buildButton(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 16.0),
        ),
        InkWell(
          onTap: () {
            if (_noWhatsApp.text.isEmpty) {
              Widget okButton = FlatButton(
                child: Text("OK"),
                onPressed: () => Navigator.pop(context),
              );
              showDialog(
                  context: context,
                  builder: (context) {
                    return AlertDialog(
                      // title: Text("Caution"),
                      content: Text("Nomor Ponsel Kosong"),
                      actions: <Widget>[okButton],
                    );
                  });
              return;
            }

            signIn(_noWhatsApp.text, tokenFcm);

            // Future.delayed(Duration(seconds: 3), () {
            //   if (statusCode == 200) {
            //     Navigator.of(context).pushReplacementNamed(UrlPalette.OTP);
            //   }
            // });

            // Navigator.of(context).pushReplacementNamed(UrlPalette.DASHBOARD);
          },
          child: Container(
            padding: EdgeInsets.only(left: 0.0, top: 5.0, bottom: 5.0),
            width: double.infinity,
            child: Text(
              'MASUK',
              style: TextStyle(color: Colors.white, fontSize: 20.0),
              textAlign: TextAlign.center,
            ),
            decoration: BoxDecoration(
              // color: Colors.red,
              borderRadius: BorderRadius.circular(10),
              // border: UnderlineInputBorder()
              border: Border.all(color: Colors.white30, width: 5),
              // enabledBorder: UnderlineInputBorder(
              //   borderSide: BorderSide(
              //     color: ColorPalette.underLineText,
              //     width: 1.5,
              //   ),
              // ),
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 16.0),
        ),
        Text(
          'or',
          style: TextStyle(
            color: Colors.white,
            fontSize: 14.0,
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 16.0),
        ),
        InkWell(
          onTap: () {
            Navigator.of(context).pushReplacementNamed(UrlPalette.REGISTER);
          },
          child: Container(
            padding: EdgeInsets.only(left: 0.0, top: 8.0, bottom: 8.0),
            width: double.infinity,
            child: Text(
              'GABUNG SEKARANG',
              style: TextStyle(color: Colors.white, fontSize: 20.0),
              textAlign: TextAlign.center,
            ),
            decoration: BoxDecoration(
              color: Colors.red[700],
              borderRadius: BorderRadius.circular(10),
            ),
          ),
        ),
        /* 
        FlatButton(
          child: Text(
            'Go Otp',
            style: TextStyle(color: Colors.white),
          ),
          onPressed: () {
            Navigator.of(context).pushReplacementNamed(UrlPalette.OTP);
          },
        ),
        FlatButton(
          child: Text(
            'Go Upload',
            style: TextStyle(color: Colors.white),
          ),
          onPressed: () {
            Navigator.of(context).pushReplacementNamed(UrlPalette.UPLOAD);
          },
        ),*/
        // FlatButton(
        //   child: Text(
        //     'Go CAMERA',
        //     style: TextStyle(color: Colors.white),
        //   ),
        //   onPressed: () {
        //     Navigator.of(context).pushReplacementNamed(UrlPalette.HOME);
        //   },
        // ),
        // FlatButton(
        //   child: Text(
        //     'Register 2',
        //     style: TextStyle(color: Colors.white),
        //   ),
        //   onPressed: () {
        //     Navigator.of(context).pushReplacementNamed(UrlPalette.HOME);
        //   },
        // ),
      ],
    );
  }

  showLoading(BuildContext context) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            backgroundColor: Color.fromRGBO(255, 97, 0, 0.1),
            // title: Text("Caution"),
            content: Center(child: CircularProgressIndicator()),
            // actions: <Widget>[okButton],
          );
        });
  }

  showAlertDialog(BuildContext context, String msg) {
    // set up the button
    Widget okButton = FlatButton(
      child: Text("OK"),
      onPressed: () => Navigator.pop(context),
    );
    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      // title: Text("Caution"),
      content: Text(msg),
      actions: [
        okButton,
      ],
    );
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  _fieldFocusChange(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }
}
