import 'dart:async';
import 'dart:convert';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:garda_digital/global.dart';
import 'package:garda_digital/screens/event/event1.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart';

final List<String> imgList = [
  'assets/images/g-sosial-bandung.png',
  'assets/images/g-sosial-bogor.png',
  'assets/images/g-sosial-jakarta.png',
  'assets/images/g-sosial-tanggerang.png',
];

class HistoryPage extends StatefulWidget {
  @override
  State createState() => new MainWidgetState();
}

class MainWidgetState extends State<HistoryPage> {
  bool _isLoading = true;
  List<dynamic> _listGSosial = List<dynamic>();
  // bool _isLoadingMore = false;
  // int currentLength = 0;
  // final int increment = 10;
  // List<dynamic> data = [];

  // Future _loadMore() async {
  //   setState(() {
  //     _isLoadingMore = true;
  //   });

  //   // Add in an artificial delay
  //   await new Future.delayed(const Duration(seconds: 2));
  //   for (var i = currentLength; i <= currentLength + increment; i++) {
  //     data.add(i);
  //   }
  //   setState(() {
  //     isLoading = false;
  //     currentLength = data.length;
  //   });
  // }

  _findHistory() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var token = "Bearer " + prefs.get(SessionString.token).toString();
    final bodyReq = {"mobilePhone": prefs.get(SessionString.noTelp)};

    ApiService.postMappingWithToken("/gsocial/history", bodyReq, token)
        .then((success) {
      if (success.statusCode == 200) {
        // Map<String, dynamic> body = jsonDecode(success.body)[0];
        setState(() {
          // _listGSosial.add(jsonDecode(success.body)[0]["gsocial"]);
          _listGSosial = jsonDecode(success.body);
          print(_listGSosial);
          _isLoading = false;
        });
        // prefs.setString(SessionString.token, body['Authorization']);
        // print(body['Authorization']);
      } else {
        prefs.clear();
        Navigator.of(context).pushReplacementNamed(UrlPalette.LOGIN);
        // showAlertDialog(context, success.statusCode.toString());
      }
      // setState(() {
      //   _isLoading = false;
      // });
    });
  }

  @override
  void initState() {
    _findHistory();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var _mediaQuery = MediaQuery.of(context);
    return Scaffold(
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(30.0), // here the desired height
          child: new AppBar(
            leading: IconButton(
              padding: EdgeInsets.only(top: 1, bottom: 2, right: 25),
              onPressed: () {
                Navigator.of(context)
                    .pushReplacementNamed(UrlPalette.DASHBOARD);
              },
              icon: Icon(
                Icons.chevron_left,
                size: 23,
                color: Colors.white,
              ), // change this size an
            ),
            title: Container(
                margin: EdgeInsets.only(right: 50),
                child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      new Text(
                        "History",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.bold),
                      )
                    ])),
            backgroundColor: Color.fromRGBO(255, 97, 0, 1),
            elevation: 0.0,
          )),
      body: Container(
          padding: EdgeInsets.only(top: 10),
          child: _loadingScreen()
              ? Center(child: CircularProgressIndicator())
              : Container(
                  child: ListView(
                    children: <Widget>[
                      _headerBanner(_mediaQuery),
                    ],
                  ),
                )),
    );
  }

  _loadingScreen() {
    if (_isLoading) {
      return true;
    }
    // if (_isLoadingSignIn) {
    //   return true;
    // }
    return false;
  }

  Widget _headerBanner(var _mediaQuery) {
    List<Widget> lines = [];
    // for (var sizeBox in imgList) {

    for (var i = 0; i < _listGSosial.length; i++) {
      // if (_listGSosial[i]['isShow'] == true) {
      lines.add(Container(

          // padding: EdgeInsets.only(top: i == imgList.length - 1 ? 0 : 15),
          padding:
              EdgeInsets.only(bottom: i == _listGSosial.length - 1 ? 0 : 15),
          child: Container(
              foregroundDecoration: BoxDecoration(
                color: _listGSosial[i]['isAvailable'] == true
                    ? Colors.transparent
                    : Colors.grey,
                backgroundBlendMode: BlendMode.saturation,
              ),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                color: Colors.white,
                boxShadow: [
                  new BoxShadow(
                    color: Colors.black,
                    offset: Offset(5, 7),
                    blurRadius: 10.0,
                  ),
                ],
              ),
              child: InkWell(
                onTap: () {
                  print(new DateFormat('dd-MMMM-yyyy').format(
                      DateTime.fromMicrosecondsSinceEpoch(
                          _listGSosial[i]['dateStart'] * 1000)));
                  print(_listGSosial[i]);
                  String _datas = jsonEncode(_listGSosial[i]);

                  if (_listGSosial[i]['isAvailable'] == true) {
                    // Navigator.of(context).pushNamed(UrlPalette.EVENT1);
                    // Navigator.pushReplacement(
                    //     context,
                    //     MaterialPageRoute(
                    //         builder: (BuildContext context) => EventOnePage(
                    //               text: "test",
                    //             )));
                    // Navigator.pushAndRemoveUntil(
                    //     context,
                    //     MaterialPageRoute(
                    //         builder: (BuildContext context) => EventOnePage(
                    //               text: "test",
                    //             )),
                    //     (Route<dynamic> route) => false);
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => EventOnePage(
                            text: jsonEncode(_listGSosial[i]),
                          ),
                        ));
                  } else {
                    Widget okButton = FlatButton(
                      child: Text("OK"),
                      onPressed: () => Navigator.pop(context),
                    );
                    showDialog(
                        context: context,
                        builder: (context) {
                          return AlertDialog(
                            content: Container(
                                // margin: EdgeInsets.only(top: 50),
                                child: Text("Event ini sudah tidak tersedia")),
                            actions: <Widget>[okButton],
                          );
                        });
                  }
                },
                child: SizedBox(
                    width: _mediaQuery.size.width - 30,
                    // child: Text(_listGSosial[i]['imageBannerName']),
                    // child: Image.network(_listGSosial[i]['imageBannerName']),

                    child: Text(_listGSosial[i]['imageBannerName'])
                    // new Image.memory(
                    //     base64.decode(_listGSosial[i]['imageBannerName'])),
                    // child: Image.asset(i > 3 ? imgList[3] : imgList[i]),
                    ),
              ))));
      // }
    }
    return Container(
        child: Column(
      children: lines,
    ));
  }
}
