import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';

import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/material.dart';
import 'package:garda_digital/global.dart';
import 'package:shared_preferences/shared_preferences.dart';

// final List<String> imgList = [
//   'https://cdn-images-1.medium.com/max/2000/1*GqdzzfB_BHorv7V2NV7Jgg.jpeg',
//   'https://cdn-images-1.medium.com/max/2000/1*wnIEgP1gNMrK5gZU7QS0-A.jpeg',
// ];
final List<String> imgList = [
  'assets/images/ads1.png',
  'assets/images/ads2.png',
  'assets/images/ads3.png',
  'assets/images/ads4.png',
  'assets/images/ads5.png',
];

final List<String> imgTopList = [
  'assets/images/ads1top.png',
  'assets/images/ads2top.png',
  'assets/images/ads3top.png',
  'assets/images/ads4top.png',
  'assets/images/ads5top.png',
];

// image: AssetImage("assets/images/pelatihan.png"),
// image: AssetImage("assets/images/instagram.png"),
// image: AssetImage("assets/images/facebook.png"),
// image: AssetImage("assets/images/more.png"),
final List<String> imgMenuList = [
  'assets/images/social-care.png',
  'assets/images/usaha.png',
  'assets/images/pelatihan.png',
  'assets/images/shield.png'
];
final List<String> imgMenu2List = [
  'assets/images/voucher.png',
  'assets/images/graduation.png',
  'assets/images/berita.png',
  'assets/images/more.png'
];
final List<String> wordMenuList = [
  'G-Sosial',
  'G-UMKM',
  'G-Training',
  'G-Insurance',
];
final List<String> wordMenu2List = [
  'G-Voucher',
  'G-Beasiswa',
  'G-News',
  'Lainnya',
];

class DashboardPage extends StatefulWidget {
  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<DashboardPage> {
  int _current = 0;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  String username;
  bool _isLoading = true;
  String _base64;

  Uint8List bytes;

  _getDataUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    username = prefs.get(SessionString.noTelp);
    var token = "Bearer " + prefs.get(SessionString.token).toString();

    new Timer(new Duration(seconds: 1), () {
      final bodyReq = {"mobilePhone": username};
      ApiService.postMappingWithToken("/findAvatarImage", bodyReq, token)
          .then((success) {
        Map<String, dynamic> body = jsonDecode(success.body);
        if (success.statusCode == 200) {
          if (body['status'] == 200) {
            setState(() {
              var _base64 = body['avatarImage'];
              bytes = base64.decode(_base64);
            });
          } else {
            logout(context);
            Navigator.of(context).pushReplacementNamed(UrlPalette.LOGIN);
          }
          // prefs.setString(SessionString.token, body['Authorization']);
          // print(body['Authorization']);
        } else {
          logout(context);
          Navigator.of(context).pushReplacementNamed(UrlPalette.LOGIN);
          // showAlertDialog(context, success.statusCode.toString());
        }
        setState(() {
          _isLoading = false;
        });
      });
    });
  }
  // final List<Widget> imageSliders = imgList
  //     .map((item) => Container(
  //           child: Container(
  //             margin: EdgeInsets.only(top: 0, left: 0, right: 0, bottom: 0),
  //             child: ClipRRect(
  //                 borderRadius: BorderRadius.all(Radius.circular(5.0)),
  //                 child: Stack(
  //                   children: <Widget>[
  //                     Image.network(item, fit: BoxFit.cover, width: 1000.0),
  //                     Positioned(
  //                       bottom: 0.0,
  //                       left: 0.0,
  //                       right: 0.0,
  //                       child: Container(
  //                         decoration: BoxDecoration(
  //                           gradient: LinearGradient(
  //                             colors: [
  //                               Color.fromARGB(200, 0, 0, 0),
  //                               Color.fromARGB(0, 0, 0, 0)
  //                             ],
  //                             begin: Alignment.bottomCenter,
  //                             end: Alignment.topCenter,
  //                           ),
  //                         ),
  //                         padding: EdgeInsets.symmetric(
  //                             vertical: 0.0, horizontal: 0.0),
  //                         child: Text(
  //                           // 'No. ${imgList.indexOf(item)} image',
  //                           '',
  //                           // style: TextStyle(
  //                           //   color: Colors.white,
  //                           //   fontSize: 20.0,
  //                           //   fontWeight: FontWeight.bold,
  //                           // ),
  //                         ),
  //                       ),
  //                     ),
  //                   ],
  //                 )),
  //           ),
  //         ))
  //     .toList();

  logout(BuildContext context) async {
    SharedPreferences _session = await SharedPreferences.getInstance();
    // _session.remove(SessionString.token);
    _session.clear();
    new Timer(new Duration(seconds: 1), () {
      Navigator.of(context).pushReplacementNamed(UrlPalette.LOGIN);
      // _isLoading = false;
    });
  }

  @override
  void initState() {
    _getDataUser();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: _loadingScreen()
          ? Center(child: CircularProgressIndicator())
          : new Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("assets/images/9.png"),
                  fit: BoxFit.fill,
                ),
              ),
              child: Stack(children: <Widget>[
                new Scaffold(
                  key: _scaffoldKey,
                  drawer: new Drawer(
                    child: new ListView(
                      children: <Widget>[
                        new DrawerHeader(
                          child: new Image.memory(bytes),
                        ),
                        // new ListTile(
                        //   title: new Text('Back To Login'),
                        //   onTap: () {
                        //     Navigator.of(context)
                        //         .pushReplacementNamed(UrlPalette.LOGIN);
                        //   },
                        // ),
                        new ListTile(
                          title: new Text('Logout'),
                          onTap: () {
                            logout(context);
                          },
                        ),
                        new Divider(),
                        // new ListTile(
                        //   title: new Text('About'),
                        //   onTap: () {},
                        // ),
                      ],
                    ),
                  ),
                  backgroundColor: Colors.transparent,
                  appBar: PreferredSize(
                      preferredSize:
                          Size.fromHeight(30.0), // here the desired height
                      child: new AppBar(
                        leading: IconButton(
                          padding: EdgeInsets.only(top: 1, bottom: 2),
                          onPressed: () =>
                              _scaffoldKey.currentState.openDrawer(),
                          icon:
                              Icon(Icons.menu, size: 23), // change this size an
                        ),
                        title: new Text(
                          "GARDA DIGITAL",
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.bold),
                        ),
                        actions: <Widget>[
                          new Padding(
                              padding: EdgeInsets.only(right: 5, bottom: 2),
                              child: Icon(
                                Icons.notifications,
                                size: 22,
                                // color: ColorPalette.white,
                              ))
                          //       new Container(),
                          //       new Center(
                          //           child: Icon(
                          //         Icons.notifications,
                          //         size: 40,
                          //         // color: ColorPalette.white,
                          //       )),
                          //     ],
                          //     title: Column(children: [
                          //       Text(
                          //         "",
                          //       ),
                        ],
                        backgroundColor: Colors.transparent,
                        elevation: 0.0,
                      )),
                  body: Container(
                    child: ListView(
                      children: <Widget>[
                        _bannerTop(),
                        Padding(
                            padding: EdgeInsets.all(5),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              mainAxisSize: MainAxisSize.max,
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: <Widget>[
                                Text("G-Coin mu :",
                                    style: TextStyle(
                                        fontSize: 11, color: Colors.white)),
                                Text("0 GC",
                                    style: TextStyle(
                                        fontSize: 18, color: Colors.white)),
                              ],
                            )),
                        _bodyOne(),
                        // getCategoryContainer(),
                        // getCategoryContainer2(),
                        _bodyTwo(),
                        // _bodyThree(),
                        _line(),
                        _info(),
                        _bannerBottom(),
                      ],
                    ),
                  ),
                ),
              ])),
      // body: Stack(
      //   children: <Widget>[
      // Scaffold(
      //   backgroundColor: Colors.transparent,
      //   appBar: new AppBar(
      //     title: new Text(
      //       "Hello World",
      //       style: TextStyle(color: Colors.amber),
      //     ),
      //     backgroundColor: Colors.transparent,
      //     elevation: 0.0,
      //   ),
      // ),
      //   ],
      // ),
    );
  }

  _loadingScreen() {
    if (_isLoading) {
      return true;
    }
    return false;
  }

  _bannerWidth(var _mediaQuery) {
    var result;
    result = _mediaQuery.size.width - 20;
    return result;
  }

  _bannerHeight(var _mediaQuery) {
    var result;
    if (_mediaQuery.size.height < 650) {
      result = _mediaQuery.size.height / 5;
    } else if (_mediaQuery.size.height > 650 && _mediaQuery.size.height < 860) {
      result = _mediaQuery.size.height / 5;
    } else {
      result = _mediaQuery.size.height / 5;
    }
    return result;
  }

  Widget _bannerBottom() {
    var _mediaQuery = MediaQuery.of(context);
    // print(_mediaQuery.size);
    return Center(
      child: SizedBox(
        height: _bannerHeight(_mediaQuery),
        width: _bannerWidth(_mediaQuery),
        child: Carousel(
          boxFit: BoxFit.cover,
          autoplay: true,
          animationCurve: Curves.fastOutSlowIn,
          animationDuration: Duration(milliseconds: 1000),
          dotSize: 6.0,
          dotIncreasedColor: Color(0xFFFF335C),
          dotBgColor: Colors.transparent,
          dotPosition: DotPosition.bottomCenter,
          dotVerticalPadding: 10.0,
          showIndicator: true,
          indicatorBgPadding: 7.0,
          borderRadius: true,
          radius: Radius.circular(10),
          images: imgList.map((url) {
            return Container(
                decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              image: DecorationImage(
                image: AssetImage(url),
                fit: BoxFit.fill,
              ),
            ));
          }).toList(),
          onImageTap: (item) {
            // Navigator.of(context).pushNamed(UrlPalette.EVENT1);
            // print(item);
          },
        ),
      ),
    );

    // images: imgList.map((url) {
    //   NetworkImage(url);
    // }).toList(),
    // );
    // return Center(
    //   child: Padding(
    //     padding: EdgeInsets.only(left: 70, right: 70),
    //     child: Column(
    //       children: <Widget>[
    //         Column(children: [
    //           CarouselSlider(
    //             items: imageSliders,
    //             options: CarouselOptions(
    //                 autoPlay: true,
    //                 enlargeCenterPage: true,
    //                 aspectRatio: 2.0,
    //                 onPageChanged: (index, reason) {
    //                   setState(() {
    //                     _current = index;
    //                   });
    //                 }),
    //           ),
    //           Row(
    //             mainAxisAlignment: MainAxisAlignment.center,
    //             children: imgList.map((url) {
    //               int index = imgList.indexOf(url);
    //               return Container(
    //                 width: 8.0,
    //                 height: 8.0,
    //                 margin:
    //                     EdgeInsets.symmetric(vertical: 1.0, horizontal: 2.0),
    //                 decoration: BoxDecoration(
    //                   shape: BoxShape.circle,
    //                   color: _current == index
    //                       ? Color.fromRGBO(0, 0, 0, 0.9)
    //                       : Color.fromRGBO(0, 0, 0, 0.4),
    //                 ),
    //               );
    //             }).toList(),
    //           ),
    //         ]),
    //       ],
    //     ),
    //   ),
    // );
  }

  Widget _bannerTop() {
    var _mediaQuery = MediaQuery.of(context);
    // print(_mediaQuery.size);
    return Center(
      child: SizedBox(
        height: _bannerHeight(_mediaQuery),
        width: _bannerWidth(_mediaQuery),
        child: Carousel(
          boxFit: BoxFit.cover,
          autoplay: true,
          animationCurve: Curves.fastOutSlowIn,
          animationDuration: Duration(milliseconds: 1000),
          dotSize: 6.0,
          dotIncreasedColor: Color(0xFFFF335C),
          dotBgColor: Colors.transparent,
          dotPosition: DotPosition.bottomCenter,
          dotVerticalPadding: 10.0,
          showIndicator: true,
          indicatorBgPadding: 7.0,
          borderRadius: true,
          radius: Radius.circular(10),
          images: imgTopList.map((url) {
            return Container(
                decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              image: DecorationImage(
                image: AssetImage(url),
                fit: BoxFit.fill,
              ),
            ));
          }).toList(),
          onImageTap: (item) {
            Navigator.of(context).pushNamed(UrlPalette.EVENT1);
            // print(item);
          },
        ),
      ),
    );
  }

  Widget _bodyOne() {
    return Padding(
        padding: EdgeInsets.only(top: 10, left: 20, bottom: 20, right: 20),
        child: new Container(
          child: Container(
            // color: Colors.white,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Padding(
                    padding: EdgeInsets.only(top: 20, bottom: 20),
                    child: Column(children: <Widget>[
                      Image(
                        image: AssetImage("assets/images/add-button-oren.png"),
                        height: 30,
                      ),
                      Text(
                        "Get G-Coin",
                        style: TextStyle(
                            color: Color.fromRGBO(255, 97, 0, 1), fontSize: 12),
                      )
                    ])),
                Padding(
                    padding: EdgeInsets.all(0),
                    child: Column(children: <Widget>[
                      Image(
                        image: AssetImage("assets/images/diamond-oren.png"),
                        height: 30,
                      ),
                      Text("Tukar G-Coin",
                          style: TextStyle(
                              color: Color.fromRGBO(255, 97, 0, 1),
                              fontSize: 12))
                    ])),
                Padding(
                    padding: EdgeInsets.only(left: 15),
                    child: Column(children: <Widget>[
                      InkWell(
                          onTap: () {
                            Navigator.of(context)
                                .pushNamed(UrlPalette.HISTORYPAGE);
                          },
                          child: Image(
                            image: AssetImage("assets/images/history-oren.png"),
                            height: 30,
                          )),
                      InkWell(
                          onTap: () {
                            Navigator.of(context)
                                .pushNamed(UrlPalette.HISTORYPAGE);
                          },
                          child: Text("History",
                              style: TextStyle(
                                  color: Color.fromRGBO(255, 97, 0, 1),
                                  fontSize: 12)))
                    ])),
              ],
            ),
            // decoration: BoxDecoration(
            //   borderRadius: BorderRadius.circular(10),
            //   color: Colors.white,
            // )
          ),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
            color: Colors.white,
            boxShadow: [
              new BoxShadow(
                color: Colors.black,
                offset: const Offset(5.0, 7.0),
                blurRadius: 8.0,
              ),
            ],

            // color: Colors.transparent,
            // border: Border(
            //   bottom: BorderSide(
            //     width: 3.0,
            //   ),
            //   right: BorderSide(
            //     width: 3.0,
            //   ),
            // ),
          ), // BoxDecoration
          // decoration: BoxDecoration(
          //   border: Border(
          //     bottom: BorderSide(
          //       width: 3.0,
          //     ),
          //     right: BorderSide(
          //       width: 3.0,
          //     ),
          //     // top: BorderSide(
          //     //   color: Colors.black,
          //     //   width: 3.0,
          //     // ),
          //     left: BorderSide(
          //       color: Colors.black,
          //       width: 3.0,
          //     ),
          //   ),
          //   borderRadius: BorderRadius.only(bottomRight: Radius.circular(10)),
          //   // borderRadius: BorderRadius.only(),
          //   // borderRadius: BorderRadius.circular(10),
          // ),
          // decoration: BoxDecoration(
          //   border: Border(
          //     left: BorderSide(
          //       color: Colors.black,
          //       width: 5.0,
          //     ),
          //     top: BorderSide(
          //       color: Colors.black,
          //       width: 5.0,
          //     ),
          //   ), //color: Colors.black, width: 5
          //   // color: Colors.white,
          //   borderRadius: BorderRadius.circular(10),
          // ),
        ));
  }

  Widget _bodyTwo() {
    return Padding(
        padding: EdgeInsets.all(0),
        child: Container(
          child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              mainAxisSize: MainAxisSize.max,
              children: new List.generate(
                imgMenuList.length,
                (index) => new Padding(
                    padding: EdgeInsets.only(top: 0, bottom: 0, left: 1),
                    child: Column(children: <Widget>[
                      InkWell(
                          onTap: () {
                            if (index == 0) {
                              Navigator.of(context)
                                  .pushNamed(UrlPalette.SOSIALPAGE);
                            }
                          },
                          child: Image(
                            image: AssetImage(imgMenuList[index]),
                            height: 30,
                          )),
                      InkWell(
                          onTap: () {
                            if (index == 0) {
                              Navigator.of(context)
                                  .pushNamed(UrlPalette.SOSIALPAGE);
                            }
                          },
                          child: Text(wordMenuList[index],
                              style: TextStyle(fontSize: 12))),
                      Padding(padding: EdgeInsets.only(top: 15)),
                      Image(
                        image: AssetImage(imgMenu2List[index]),
                        height: 30,
                      ),
                      Text(
                        wordMenu2List[index],
                        style: TextStyle(fontSize: 12),
                      )
                    ])),
              )),
        ));
  }

  // Widget _bodyTwo() {
  //   return new Row(
  //       mainAxisAlignment: MainAxisAlignment.center,
  //       mainAxisSize: MainAxisSize.max,
  //       // crossAxisAlignment: CrossAxisAlignment.stretch,
  //       children: new List.generate(
  //           imgMenuList.length,
  //           (index) => new Container(
  //               // color: Colors.amber,
  //               margin: EdgeInsets.only(right: 1, left: 1), //, bottom: 20),
  //               height: 60,
  //               width: 90,
  //               padding: EdgeInsets.only(top: 10),
  //               decoration: BoxDecoration(
  //                 color: Colors.transparent,
  //                 borderRadius: BorderRadius.all(Radius.circular(15)),
  //               ),
  //               child: Column(children: <Widget>[
  //                 Image(
  //                   image: AssetImage(imgMenuList[index]),
  //                   height: 30,
  //                 ),
  //                 Text(
  //                   wordMenuList[index],
  //                   style: TextStyle(fontSize: 14),
  //                 )
  //               ]))));
  // }

  // Widget _bodyThree() {
  //   return new Row(
  //       mainAxisAlignment: MainAxisAlignment.center,
  //       children: new List.generate(
  //           imgMenuList.length,
  //           (index) => new Container(
  //               margin: EdgeInsets.only(right: 5, left: 5), //, bottom: 20),
  //               height: 60,
  //               width: 90,
  //               padding: EdgeInsets.only(top: 10),
  //               decoration: BoxDecoration(
  //                 color: Colors.transparent,
  //                 borderRadius: BorderRadius.all(Radius.circular(15)),
  //               ),
  //               child: Column(children: <Widget>[
  //                 Image(
  //                   image: AssetImage(imgMenu2List[index]),
  //                   height: 30,
  //                 ),
  //                 Text(wordMenu2List[index])
  //               ]))));
  // }

  // Widget _bodyTwo() {
  //   return Padding(
  //       padding: EdgeInsets.all(0),
  //       child: Container(
  //         child: Row(
  //           mainAxisAlignment: MainAxisAlignment.center,
  //           mainAxisSize: MainAxisSize.max,
  //           children: <Widget>[
  //             Padding(
  //                 padding: EdgeInsets.only(left: 20),
  //                 // padding: EdgeInsets.only(top: 0, bottom: 0, left: 40),
  //                 child: Column(children: <Widget>[
  //                   Image(
  //                     image: AssetImage("assets/images/social-care.png"),
  //                     height: 30,
  //                   ),
  //                   Text("G-Sosial")
  //                 ])),
  //             Padding(
  //                 // padding: EdgeInsets.all(0),
  //                 padding: EdgeInsets.only(top: 0, bottom: 0, left: 45),
  //                 child: Column(children: <Widget>[
  //                   Image(
  //                     image: AssetImage("assets/images/usaha.png"),
  //                     height: 30,
  //                   ),
  //                   Text("G-MSME")
  //                 ])),
  //             Padding(
  //                 // padding: EdgeInsets.all(0),
  //                 padding: EdgeInsets.only(top: 0, bottom: 0, left: 45),
  //                 child: Column(children: <Widget>[
  //                   Image(
  //                     image: AssetImage("assets/images/cashless.png"),
  //                     height: 30,
  //                   ),
  //                   Text("G-Pay")
  //                 ])),
  //             Padding(
  //                 // padding: EdgeInsets.all(0),
  //                 padding: EdgeInsets.only(top: 0, bottom: 0, left: 45),
  //                 child: Column(children: <Widget>[
  //                   Image(
  //                     image: AssetImage("assets/images/asuransi.png"),
  //                     height: 30,
  //                   ),
  //                   Text("G-Insurance")
  //                 ])),
  //           ],
  //         ),
  //       ));
  // }

  // Widget _bodysTwo() {
  //   return Padding(
  //       padding: EdgeInsets.all(0),
  //       child: Container(
  //         child: Row(
  //           mainAxisAlignment: MainAxisAlignment.center,
  //           mainAxisSize: MainAxisSize.max,
  //           children: <Widget>[
  //             Padding(
  //                 padding: EdgeInsets.only(left: 20),
  //                 // padding: EdgeInsets.only(top: 0, bottom: 0, left: 40),
  //                 child: Column(children: <Widget>[
  //                   Image(
  //                     image: AssetImage("assets/images/pelatihan.png"),
  //                     height: 30,
  //                   ),
  //                   Text("G-Sosial")
  //                 ])),
  //             Padding(
  //                 // padding: EdgeInsets.all(0),
  //                 padding: EdgeInsets.only(top: 0, bottom: 0, left: 45),
  //                 child: Column(children: <Widget>[
  //                   Image(
  //                     image: AssetImage("assets/images/usaha.png"),
  //                     height: 30,
  //                   ),
  //                   Text("G-MSME")
  //                 ])),
  //             Padding(
  //                 // padding: EdgeInsets.all(0),
  //                 padding: EdgeInsets.only(top: 0, bottom: 0, left: 45),
  //                 child: Column(children: <Widget>[
  //                   Image(
  //                     image: AssetImage("assets/images/cashless.png"),
  //                     height: 30,
  //                   ),
  //                   Text("G-Pay")
  //                 ])),
  //             Padding(
  //                 // padding: EdgeInsets.all(0),
  //                 padding: EdgeInsets.only(top: 0, bottom: 0, left: 45),
  //                 child: Column(children: <Widget>[
  //                   Image(
  //                     image: AssetImage("assets/images/asuransi.png"),
  //                     height: 30,
  //                   ),
  //                   Text("G-Insurance")
  //                 ])),
  //           ],
  //         ),
  //       ));
  // }

  // Widget _bodyThree() {
  //   return Padding(
  //       padding: EdgeInsets.all(0),
  //       child: Container(
  //         child: Row(
  //           mainAxisAlignment: MainAxisAlignment.center,
  //           mainAxisSize: MainAxisSize.max,
  //           children: <Widget>[
  //             Padding(
  //                 padding: EdgeInsets.only(top: 20, bottom: 0, left: 6),
  //                 child: Column(children: <Widget>[
  //                   Image(
  //                     image: AssetImage("assets/images/pelatihan.png"),
  //                     height: 30,
  //                   ),
  //                   Text("G-Training")
  //                 ])),
  //             Padding(
  //                 padding: EdgeInsets.only(top: 20, bottom: 0, left: 6),
  //                 child: Column(children: <Widget>[
  //                   Image(
  //                     image: AssetImage("assets/images/instagram.png"),
  //                     height: 30,
  //                   ),
  //                   Text("G-Instagram")
  //                 ])),
  //             Padding(
  //                 padding: EdgeInsets.only(top: 20, bottom: 0, left: 0),
  //                 child: Column(children: <Widget>[
  //                   Image(
  //                     image: AssetImage("assets/images/facebook.png"),
  //                     height: 30,
  //                   ),
  //                   Text("G-Facebook")
  //                 ])),
  //             Padding(
  //                 padding: EdgeInsets.only(top: 20, bottom: 0, left: 0),
  //                 child: Column(children: <Widget>[
  //                   Image(
  //                     image: AssetImage("assets/images/more.png"),
  //                     height: 30,
  //                   ),
  //                   Text("Lainnya")
  //                 ])),
  //           ],
  //         ),
  //       ));
  // }

  Widget _line() {
    return Padding(
        padding: EdgeInsets.only(top: 5, left: 15, right: 15),
        child: Container(
          child: Text(""),
          decoration: BoxDecoration(
            border: Border(bottom: BorderSide(width: 2, color: Colors.grey)),
          ),
        ));
  }

  Widget _info() {
    return Padding(
        padding: EdgeInsets.only(top: 5, left: 15, right: 15, bottom: 40),
        child: Container(
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
              Text(
                "Info Promo dan Spesial",
                style: TextStyle(
                  fontSize: 17,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text(
                "Lihat Semua",
                style: TextStyle(fontSize: 10, color: Colors.red),
              )
            ])));
  }
}
