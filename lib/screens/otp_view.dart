import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:garda_digital/global.dart';
import 'package:shared_preferences/shared_preferences.dart';

class OtpPage extends StatefulWidget {
  @override
  State createState() => new MainWidgetState();
}

class MainWidgetState extends State<OtpPage> {
  final FocusNode _aFocuse = FocusNode();
  final FocusNode _bFocuse = FocusNode();
  final FocusNode _cFocuse = FocusNode();
  final FocusNode _dFocuse = FocusNode();

  final aBox = TextEditingController();
  final bBox = TextEditingController();
  final cBox = TextEditingController();
  final dBox = TextEditingController();
  String username;
  List<String> otp;
  String otps;

  _getUsernameAndOtp(BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    username = prefs.get(SessionString.noTelp);
    // prefs.setString('otp', '5432');
    otps = prefs.get(SessionString.otp);

    // new Timer(new Duration(seconds: 2), () {
    //   if (prefs.get(SessionString.fcmOtp) == null) {
    //     return;
    //   }
    //   otp = prefs.get(SessionString.fcmOtp).split("");
    //   setState(() {
    //     aBox.text = otp[0];
    //     bBox.text = otp[1];
    //     cBox.text = otp[2];
    //     dBox.text = otp[3];
    //   });
    // });

    // new Timer(new Duration(seconds: 3), () {
    //   final bodyReq = {
    //     "username": username,
    //     "password": prefs.get(SessionString.fcmOtp)
    //   };
    //   ApiService.postMapping("/auth", bodyReq).then((success) {
    //     Map<String, dynamic> body = jsonDecode(success.body);
    //     if (success.statusCode == 200) {
    //       prefs.setString(SessionString.token, body['Authorization']);
    //       print(body['Authorization']);
    //       Navigator.of(context).pushReplacementNamed(UrlPalette.DASHBOARD);
    //     } else {
    //       // showAlertDialog(context, success.statusCode.toString());
    //     }
    //   });
    // });

    // final bodyReq = {
    //   // "username": _noWhatsApp.text,
    //   "password": "5555"
    // };
    // ApiService.postMapping("/auth", bodyReq).then((success) {
    //   if (success.statusCode == 200) {
    //   } else {
    //     // showAlertDialog(context, success.statusCode.toString());
    //   }
    // });
  }

  _saveSession(var _body) async {
    Map<String, dynamic> body = jsonDecode(_body.body);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(SessionString.token, body['Authorization']);
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _getUsernameAndOtp(context);
    // setState(() {
    //   // aBox.text = "3";
    //   // bBox.text = "2";
    //   // cBox.text = "1";
    //   // dBox.text = "7";
    // });
    return Scaffold(
      body: _body(context),
    );
  }

  // -----------------------------
  showLoadingBar(BuildContext context) {
    // set up the button
    // Widget okButton = FlatButton(
    //   child: Text("OK"),
    //   onPressed: () => Navigator.pop(context),
    //   // onPressed: () => Navigator.pop(context),
    // );
    // set up the AlertDialog
    // if (_loadingRegister == true) {
    AlertDialog alert = AlertDialog(
      backgroundColor: Colors.transparent,

      // title: Text("Caution"),
      // content: Padding(
      //     padding: EdgeInsets.only(left: 10),
      //     child: CircularProgressIndicator()),
      content: Container(
          padding: EdgeInsets.only(left: 10),
          margin: EdgeInsets.only(right: 100, left: 100),
          height: 20,
          width: 20,
          child: CircularProgressIndicator(
            backgroundColor: Colors.transparent,
          )),
      // actions: [
      //   okButton,
      // ],
    );
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
    // }
  }

  Widget _body(BuildContext context) {
    return Column(
      children: <Widget>[
        Expanded(
          child: _topLayout(context),
        ),
        _bottomButton(context)
      ],
    );
  }

  Widget _boxWithLable(BuildContext context) {
    return Column(
      children: <Widget>[
        SizedBox(
          height: 20,
        ),
        Container(
            margin: EdgeInsets.symmetric(vertical: 10, horizontal: 25),
            alignment: Alignment.centerLeft,
            child: Text("OTP Code:",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                    color: Color.fromRGBO(255, 97, 0, 1)))),
        _boxBuilder(context)
      ],
    );
  }

  Widget _boxBuilder(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        _box(context, _aFocuse, _bFocuse, aBox, bBox, _aFocuse),
        _box(context, _bFocuse, _cFocuse, bBox, cBox, _aFocuse),
        _box(context, _cFocuse, _dFocuse, cBox, dBox, _bFocuse),
        _box(context, _dFocuse, null, dBox, null, _cFocuse),
      ],
    );
  }

  Widget _box(
      BuildContext context,
      FocusNode focusNode,
      FocusNode toFocus,
      TextEditingController txt,
      TextEditingController nextText,
      FocusNode replaceFocus) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 5, horizontal: 3),
      alignment: Alignment.center,
      height: 50,
      width: 50,
      child: TextField(
        controller: txt,
        focusNode: focusNode,
        onChanged: (value) {
          if (!value.isEmpty) {
            print(value);
            _fieldFocusChange(context, focusNode, toFocus);
            _fieldReplaceChange(nextText);
          } else {
            if (value.isEmpty) {
              _fieldFocusChange(context, focusNode, replaceFocus);
            }
          }
        },
        keyboardType: TextInputType.number,
        maxLength: 1,
        textAlign: TextAlign.center,
        decoration: InputDecoration(border: InputBorder.none, counterText: ''),
      ),
      decoration: BoxDecoration(
          border: Border.all(color: Color.fromRGBO(255, 97, 0, 1))),
    );
  }

  Widget _headerImage() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 30, horizontal: 20),
      child: Image.asset(
        "assets/images/logo_gdi_tameng.png",
        // "assets/someimage.png",
        fit: BoxFit.cover,
        height: 160,
      ),
    );
  }

  Widget _bottomButton(BuildContext context) {
    return Container(
      alignment: Alignment.topCenter,
      child: Column(
        children: <Widget>[
          RaisedButton(
            color: Color.fromRGBO(255, 97, 0, 1),
            onPressed: () {
              print('clicked');
              final bodyReq = {
                "username": username,
                "password": aBox.text + bBox.text + cBox.text + dBox.text
              };
              ApiService.postMapping("/auth", bodyReq).then((success) {
                if (success.statusCode == 200) {
                  _saveSession(success);
                  Navigator.of(context)
                      .pushReplacementNamed(UrlPalette.DASHBOARD);
                } else {
                  print("401");
                  Widget okButton = FlatButton(
                    child: Text("OK"),
                    onPressed: () => Navigator.pop(context),
                  );
                  showDialog(
                      context: context,
                      builder: (context) {
                        return AlertDialog(
                          // title: Text("Caution"),
                          content: Text("Kode OTP tidak valid"),
                          actions: <Widget>[okButton],
                        );
                      });
                  return;
                  // showAlertDialog(context, success.statusCode.toString());
                }
              });
              // showLoadingBar(context);
              // Navigator.of(context).pushReplacementNamed(UrlPalette.REGISTER);
            },
            child: Text(
              'Submit',
              style: TextStyle(color: Colors.white),
            ),
          ),
          SizedBox(
            height: 20,
          ),
        ],
      ),
    );
  }

  Widget _topLayout(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[_headerImage(), _boxWithLable(context)],
      ),
    );
  }

  _fieldFocusChange(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    if (!dBox.text.isEmpty) {
      if (!aBox.text.isEmpty &&
          !bBox.text.isEmpty &&
          !cBox.text.isEmpty &&
          !dBox.text.isEmpty) {
        final bodyReq = {
          "username": username,
          "password": aBox.text + bBox.text + cBox.text + dBox.text
        };
        ApiService.postMapping("/auth", bodyReq).then((success) {
          if (success.statusCode == 200) {
            _saveSession(success);
            Navigator.of(context).pushReplacementNamed(UrlPalette.DASHBOARD);
          } else {
            // showAlertDialog(context, success.statusCode.toString());
          }
        });
      }
    }
    FocusScope.of(context).requestFocus(nextFocus);
  }

  _fieldReplaceChange(TextEditingController nextText) {
    nextText.text = "";
  }
}
