import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:garda_digital/global.dart';
import 'package:garda_digital/screens/login_view.dart';
import 'dart:convert';

import 'package:garda_digital/screens/register_2_view.dart';

class RegisterPage extends StatefulWidget {
  String text;

  RegisterPage({Key key, this.text}) : super(key: key);

  @override
  _registerState createState() => _registerState();
}

class _registerState extends State<RegisterPage> {
  final _noWhatsApp = TextEditingController();
  final _komunitas = TextEditingController();
  // final _statusPerkawinan = TextEditingController();
  final _jumlahAnak = TextEditingController();
  // final _jenisKendaraan = TextEditingController();
  final _merkKendaraan = TextEditingController();
  final _tipeKendaraan = TextEditingController();
  final _tahunPembuatan = TextEditingController();
  final _noPol = TextEditingController();
  String _valStatusPerkawinan;
  String _caution;
  final _listStatusPerkawinan = [
    "Belum Menikah",
    "Menikah",
    "Berpisah",
  ];
  String _valJenisKendaraan;
  final _listJenisKendaraan = [
    "Roda 2",
    "Roda 4",
  ];
  int selectedIndex1 = 0;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        leading: Builder(
          builder: (BuildContext context) {
            return InkWell(
              onTap: () {
                Navigator.of(context).pushReplacementNamed(UrlPalette.LOGIN);
              },
              child: Container(
                padding: EdgeInsets.only(left: 0.0, top: 5.0, bottom: 5.0),
                width: double.infinity,
                child: const Icon(Icons.chevron_left),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(0),
                ),
              ),
            );
            // return IconButton(
            //   icon: const Icon(Icons.chevron_left),
            //   splashColor: Colors.transparent,
            //   highlightColor: Colors.transparent,
            //   // onPressed: () => Navigator.pushNamed(context, "/"),
            //   onPressed: () {
            //     Navigator.push(
            //         context,
            //         MaterialPageRoute(
            //           builder: (context) => LoginPage(),
            //         ));
            //   },
            // );
          },
        ),
        title: new Text(
          "Register",
          style: TextStyle(color: Colors.white),
        ),
        // backgroundColor: Colors.redAccent,
        backgroundColor: Color.fromRGBO(255, 97, 0, 1),
        // backgroundColor: Colors.amber[700],
        elevation: 0.0,
      ),
      body: Container(
        // decoration: BoxDecoration(
        //   image: DecorationImage(
        //     image: AssetImage("assets/images/BG.jpeg"),
        //     fit: BoxFit.fill,
        //   ),
        // ),
        padding: EdgeInsets.all(20.0),
        child: ListView(
          children: <Widget>[
            Center(
              child: Column(
                children: <Widget>[
                  _iconRegister(),
                  _titleDescription(),
                  _textField(context),
                  _buildButton(context),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _iconRegister() {
    return Image.asset(
      "assets/images/akun.png",
      width: 150.0,
      height: 150.0,
    );
  }

  Widget _titleDescription() {
    return Column(
      children: <Widget>[],
    );
  }

  Widget _textField(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 12.0),
        ),
        TextFormField(
          keyboardType: TextInputType.number,
          controller: _noWhatsApp,
          inputFormatters: [
            LengthLimitingTextInputFormatter(16),
          ],
          decoration: const InputDecoration(
            border: UnderlineInputBorder(),
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: ColorPalette.underLineText,
                width: 1.5,
              ),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: ColorPalette.underLineText,
                width: 3.0,
              ),
            ),
            labelText: "No.WhatsApp",
            labelStyle: TextStyle(color: ColorPalette.textColor),
          ),
          style: TextStyle(color: ColorPalette.textColor),
          autofocus: false,
        ),
        TextFormField(
          controller: _komunitas,
          decoration: const InputDecoration(
            border: UnderlineInputBorder(),
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: ColorPalette.underLineText,
                width: 1.5,
              ),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: ColorPalette.underLineText,
                width: 3.0,
              ),
            ),
            labelText: "Komunitas",
            labelStyle: TextStyle(color: ColorPalette.textColor),
          ),
          style: TextStyle(color: ColorPalette.textColor),
          autofocus: false,
        ),
        Theme(
            data: new ThemeData(
                canvasColor: Color.fromRGBO(255, 97, 0, 1),
                primaryColor: Colors.black,
                accentColor: Colors.black,
                hintColor: Colors.black),
            child: DropdownButtonFormField<String>(
              //isDense: true,
              decoration: const InputDecoration(
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(
                    color: ColorPalette.underLineText,
                    width: 1.5,
                  ),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(
                    color: ColorPalette.underLineText,
                    width: 3.0,
                  ),
                ),
              ),
              hint: Text('Status Perkawinan',
                  style: TextStyle(color: ColorPalette.textColor)),
              value: _valStatusPerkawinan,
              icon: Icon(Icons.check_circle_outline,
                  color: ColorPalette.textColor),
              iconSize: 24,
              elevation: 26,
              style: TextStyle(color: ColorPalette.textColor),
              onChanged: (String newValue) {
                setState(() {
                  _valStatusPerkawinan = newValue;
                });
              },
              items: _listStatusPerkawinan.map((value) {
                return DropdownMenuItem(
                  child: Text(value),
                  value: value,
                );
              }).toList(),
            )),
        TextFormField(
          keyboardType: TextInputType.number,
          // maxLength: 1,
          inputFormatters: [
            LengthLimitingTextInputFormatter(1),
          ],
          controller: _jumlahAnak,
          decoration: const InputDecoration(
            border: UnderlineInputBorder(),
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: ColorPalette.underLineText,
                width: 1.5,
              ),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: ColorPalette.underLineText,
                width: 3.0,
              ),
            ),
            labelText: "Jumlah Anak",
            labelStyle: TextStyle(color: ColorPalette.textColor),
          ),
          style: TextStyle(color: ColorPalette.textColor),
          autofocus: false,
        ),
        Theme(
            data: new ThemeData(
                canvasColor: Color.fromRGBO(255, 97, 0, 1),
                primaryColor: Colors.black,
                accentColor: Colors.black,
                hintColor: Colors.black),
            child: DropdownButtonFormField<String>(
              //isDense: true,
              decoration: const InputDecoration(
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(
                    color: ColorPalette.underLineText,
                    width: 1.5,
                  ),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(
                    color: ColorPalette.underLineText,
                    width: 3.0,
                  ),
                ),
              ),
              hint: Text('Jenis Kendaraan Bermotor',
                  style: TextStyle(color: ColorPalette.textColor)),
              value: _valJenisKendaraan,
              icon: Icon(Icons.check_circle_outline,
                  color: ColorPalette.textColor),
              iconSize: 24,
              elevation: 26,
              style: TextStyle(color: ColorPalette.textColor),
              onChanged: (String newValue) {
                setState(() {
                  _valJenisKendaraan = newValue;
                });
              },
              items: _listJenisKendaraan.map((value) {
                return DropdownMenuItem(
                  child: Text(value),
                  value: value,
                );
              }).toList(),
            )),
        TextFormField(
          controller: _merkKendaraan,
          decoration: const InputDecoration(
            border: UnderlineInputBorder(),
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: ColorPalette.underLineText,
                width: 1.5,
              ),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: ColorPalette.underLineText,
                width: 3.0,
              ),
            ),
            labelText: "Merk Kendaraan Motor",
            labelStyle: TextStyle(color: ColorPalette.textColor),
          ),
          style: TextStyle(color: ColorPalette.textColor),
          autofocus: false,
        ),
        TextFormField(
          controller: _tipeKendaraan,
          decoration: const InputDecoration(
            border: UnderlineInputBorder(),
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: ColorPalette.underLineText,
                width: 1.5,
              ),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: ColorPalette.underLineText,
                width: 3.0,
              ),
            ),
            labelText: "Tipe",
            labelStyle: TextStyle(color: ColorPalette.textColor),
          ),
          style: TextStyle(color: ColorPalette.textColor),
          autofocus: false,
        ),
        TextFormField(
          keyboardType: TextInputType.number,
          // maxLength: 4,
          inputFormatters: [
            LengthLimitingTextInputFormatter(4),
          ],
          controller: _tahunPembuatan,
          decoration: const InputDecoration(
            border: UnderlineInputBorder(),
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: ColorPalette.underLineText,
                width: 1.5,
              ),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: ColorPalette.underLineText,
                width: 3.0,
              ),
            ),
            labelText: "Tahun Pembuatan",
            labelStyle: TextStyle(color: ColorPalette.textColor),
          ),
          style: TextStyle(color: ColorPalette.textColor),
          autofocus: false,
        ),
        TextFormField(
          // maxLength: 10,
          inputFormatters: [LengthLimitingTextInputFormatter(15)],
          controller: _noPol,
          decoration: const InputDecoration(
            border: UnderlineInputBorder(),
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: ColorPalette.underLineText,
                width: 1.5,
              ),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: ColorPalette.underLineText,
                width: 3.0,
              ),
            ),
            labelText: "No.Pol STNK",
            labelStyle: TextStyle(color: ColorPalette.textColor),
          ),
          style: TextStyle(color: ColorPalette.textColor),
          autofocus: false,
          // onChanged: (value) {
          //   RegExp regAlp = new RegExp(
          //     // r"^WS{1,2}:\/\/\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}:56789",
          //     // r'[A-Z]',
          //     // r'[!@#<>?":_`~;[\]\\|=+)(*&^%0-9-]',
          //     r'^[a-z]+$',
          //     caseSensitive: false,
          //     multiLine: false,
          //   );

          //   RegExp regNum = new RegExp(
          //     // r"^WS{1,2}:\/\/\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}:56789",
          //     // r'[A-Z]',
          //     // r'[!@#<>?":_`~;[\]\\|=+)(*&^%0-9-]',
          //     r'^[0-9]',
          //     caseSensitive: false,
          //     multiLine: false,
          //   );
          //   var val;
          //   if (value.length == 1) {
          //     val = value.substring(0);
          //     if (regAlp.hasMatch(val)) {
          //       //do nothing
          //       // print("REGEX 1 ALP " + val);
          //     } else {
          //       setState(() {
          //         _noPol.text = "";
          //       });
          //     }
          //   } else if (value.length > 1 && value.length < 6) {
          //     val = value.substring(value.length - 1, value.length);
          //     if (regNum.hasMatch(val)) {
          //       //do nothing
          //       // print("REGEX 2 NUM " + val);
          //     } else {
          //       setState(() {
          //         _noPol.text = value.substring(0, value.length - 1);
          //         _noPol.selection = TextSelection(
          //             baseOffset: _noPol.text.length,
          //             extentOffset: _noPol.text.length);
          //       });
          //     }
          //   } else if (value.length > 5) {
          //     val = value.substring(5, value.length);
          //     if (regAlp.hasMatch(val)) {
          //       // print("REGEX 1 ALP " + val);
          //     } else {
          //       setState(() {
          //         _noPol.text = value.substring(0, value.length - 1);
          //         _noPol.selection = TextSelection(
          //             baseOffset: _noPol.text.length,
          //             extentOffset: _noPol.text.length);
          //       });
          //     }
          //   }
          // },
        ),
        Padding(
          padding: EdgeInsets.only(top: 10.0),
        ),
      ],
    );
  }

  List<Widget> _buildItems1() {
    return _listStatusPerkawinan
        .map((val) => MySelectionItem(
              title: val,
            ))
        .toList();
  }

  showAlertDialog(BuildContext context) {
    // set up the button
    Widget okButton = FlatButton(
      child: Text("OK"),
      onPressed: () => Navigator.pop(context),
    );
    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      // title: Text("Caution"),
      content: Text(_caution),
      actions: [
        okButton,
      ],
    );
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  Widget _buildButton(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 16.0),
        ),
        InkWell(
          onTap: () {
            print('clicked');
            var _error = [];
            if (_noWhatsApp.text == "") {
              _error.add("No WhatsApp");
              // _caution = "Mohon isi No WhatsApp ";
              // showAlertDialog(context);
              // return;
            }
            if (_valStatusPerkawinan == null) {
              _error.add("Status Perkawinan");
            }
            if (_merkKendaraan.text == "") {
              _error.add("Merk Kendaraan");
            }
            if (_tahunPembuatan.text == "") {
              _error.add("Tahun Permbuatan");
            }
            if (_noPol.text == "") {
              _error.add("No.Pol STNK");
            }

            if (_error.length > 0) {
              _caution = "Mohon isi \n- ";
              int num = 1;
              for (var obj in _error) {
                if (_error.length == num) {
                  _caution += obj;
                  //"dan " + obj;
                } else {
                  _caution += obj + " \n- ";
                }
                num++;
              }
              showAlertDialog(context);
              return;
            }

            String body = jsonEncode({
              "mobilePhone": _noWhatsApp.text,
              "komunitas": _komunitas.text,
              "statusPerkawinan": _valStatusPerkawinan,
              "jumlahAnak": _jumlahAnak.text,
              "jenisKendaraan": _valJenisKendaraan,
              "merkKendaraan": _merkKendaraan.text,
              "tipeKendaraan": _tipeKendaraan.text,
              "tahunPembuatan": _tahunPembuatan.text,
              "noPol": _noPol.text,
            });
            Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => RegisterPage2(
                    text: body,
                  ),
                ));
            // Navigator.of(context).pushReplacementNamed(UrlPalette.REGISTER);
            // ApiService.registerApi(body).then((success) {
            //   if (success) {
            //     showDialog(
            //       builder: (context) => AlertDialog(
            //         title: Text('Employee has been added!!!'),
            //         actions: <Widget>[
            //           FlatButton(
            //             onPressed: () {
            //               Navigator.pop(context);
            //             },
            //             child: Text('OK'),
            //           )
            //         ],
            //       ),
            //       context: context,
            //     );
            //     return;
            //   } else {
            //     showDialog(
            //       builder: (context) => AlertDialog(
            //         title: Text('Error Adding Employee!!!'),
            //         actions: <Widget>[
            //           FlatButton(
            //             onPressed: () {
            //               Navigator.pop(context);
            //             },
            //             child: Text('OK'),
            //           )
            //         ],
            //       ),
            //       context: context,
            //     );
            //     return;
            //   }
            // });
          },
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 8.0),
            width: double.infinity,
            child: Text(
              'Berikutnya',
              style: TextStyle(color: Colors.white, fontSize: 20),
              textAlign: TextAlign.center,
            ),
            decoration: BoxDecoration(
              color: Color.fromRGBO(255, 97, 0, 1),
              borderRadius: BorderRadius.circular(30.0),
            ),
          ),
        ) /* ,
        Padding(
          padding: EdgeInsets.only(top: 16.0),
        ),
        Text(
          'or',
          style: TextStyle(
            color: Colors.white,
            fontSize: 12.0,
          ),
        ),
        FlatButton(
          child: Text(
            'Login',
            style: TextStyle(color: Colors.white),
          ),
          onPressed: () {
            Navigator.pushNamed(context, "/");
          },
        ),
        FlatButton(
          child: Text(
            'Login',
            style: TextStyle(color: Colors.white),
          ),
          onPressed: () {
            Navigator.pushNamed(context, "/");
          },
        ), */
      ],
    );
  }
}
