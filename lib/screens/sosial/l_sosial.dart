import 'dart:async';
import 'dart:convert';
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:garda_digital/global.dart';
import 'package:garda_digital/screens/event/event1.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart';

final List<String> imgList = [
  'assets/images/g-sosial-bandung.png',
  'assets/images/g-sosial-bogor.png',
  'assets/images/g-sosial-jakarta.png',
  'assets/images/g-sosial-tanggerang.png',
];

class SosialPage extends StatefulWidget {
  @override
  State createState() => new MainWidgetState();
}

class MainWidgetState extends State<SosialPage> {
  var refreshKey = GlobalKey<RefreshIndicatorState>();
  bool _isLoading = true;

  bool _isLoadingList = true;
  List<dynamic> _listGSosial;
  // bool _isLoadingMore = false;
  int currentLength = 0;
  final int increment = 5;
  List<dynamic> data = [];

  bool _hasMore = true;

  void _loadMore() async {
    // _isLoadingList = true;
    // setState(() {
    //   _isLoadingList = true;
    // });

    // Add in an artificial delay

    var _dataBulk = [];
    if (currentLength > _listGSosial.length - 1) {
      setState(() {
        _isLoadingList = false;
        _hasMore = false;
      });
    } else {
      await new Future.delayed(const Duration(seconds: 1));
      print(currentLength);
      for (var i = currentLength; i <= currentLength + increment; i++) {
        if (i > _listGSosial.length - 1) {
          //do nothing
        } else {
          print(i);
          _dataBulk.add(_listGSosial[i]);
        }
      }
    }

    setState(() {
      _isLoadingList = false;
      data.addAll(_dataBulk);
      currentLength = data.length;
    });
  }

  _findActiveGsocial() async {
    currentLength = 0;
    data = [];
    _isLoading = true;
    _hasMore = true;
    refreshKey.currentState?.show(atTop: false);
    // await Future.delayed(Duration(seconds: 1));
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var token = "Bearer " + prefs.get(SessionString.token).toString();
    ApiService.getMappingWithToken("/gsocial/findActiveGsocial", token)
        .then((success) {
      if (success.statusCode == 200) {
        List<dynamic> body = jsonDecode(success.body);
        setState(() {
          body.sort((a, b) {
            if (a['isAvailable'] == b['isAvailable']) {
              return 0;
            }
            if (a['isAvailable']) {
              return -1;
            }
            if (b['isAvailable']) {
              return 1;
            }
          });
          _listGSosial = body;
          _isLoading = false;
          _loadMore();
        });
        // prefs.setString(SessionString.token, body['Authorization']);
        // print(body['Authorization']);
      } else {
        prefs.clear();
        Navigator.of(context).pushReplacementNamed(UrlPalette.LOGIN);
        // showAlertDialog(context, success.statusCode.toString());
      }
      // setState(() {
      //   _isLoading = false;
      // });
    });
  }

  Future<Null> refreshList() async {
    refreshKey.currentState?.show(atTop: false);
    // await Future.delayed(Duration(seconds: 1));
    _findActiveGsocial();

    return null;
  }

  @override
  void initState() {
    super.initState();
    _findActiveGsocial();
    refreshList();
  }

  @override
  Widget build(BuildContext context) {
    var _mediaQuery = MediaQuery.of(context);
    return Scaffold(
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(30.0), // here the desired height
            child: new AppBar(
              leading: IconButton(
                padding: EdgeInsets.only(top: 1, bottom: 2, right: 25),
                onPressed: () {
                  Navigator.of(context)
                      .pushReplacementNamed(UrlPalette.DASHBOARD);
                },
                icon: Icon(
                  Icons.chevron_left,
                  size: 23,
                  color: Colors.white,
                ), // change this size an
              ),
              title: Container(
                  margin: EdgeInsets.only(right: 50),
                  child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        new Text(
                          "G-Social",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.bold),
                        )
                      ])),
              backgroundColor: Color.fromRGBO(255, 97, 0, 1),
              elevation: 0.0,
            )),
        body: RefreshIndicator(
            key: refreshKey,
            onRefresh: refreshList,
            child: Container(
                padding: EdgeInsets.only(top: 10),
                child: _loadingScreen()
                    ? Center(child: CircularProgressIndicator())
                    : Container(
                        child: Container(
                            child: ListView.builder(
                                itemCount:
                                    _hasMore ? data.length + 1 : data.length,
                                itemBuilder: (context, index) {
                                  print(index);
                                  print(data.length);

                                  print(_listGSosial.length);
                                  if (index >= data.length) {
                                    if (!_isLoadingList) {
                                      _loadMore();
                                    }
                                    if (data.length == _listGSosial.length) {
                                      return Container();
                                    } else {
                                      return Container(
                                        child: Center(
                                            child: CircularProgressIndicator()),
                                        padding: EdgeInsets.only(bottom: 20),
                                      );
                                    }
                                  } else {
                                    return _headerBanner(_mediaQuery, index);
                                  }
                                  // return ListTile(
                                  //   leading: Text(index.toString()),
                                  //   title: Text(data[index]['imageBannerName']),
                                  // );
                                  // print("i " + position.toString());
                                  // print(
                                  //     "data.length " + data.length.toString());
                                  // int validationLoading;
                                  // if (data.length == _listGSosial.length) {
                                  //   validationLoading = data.length;
                                  // } else {
                                  //   validationLoading = data.length - 1;
                                  // }

                                  // if (position == validationLoading) {
                                  //   _loadMore();
                                  //   return Container(
                                  //     child: Center(
                                  //         child: CircularProgressIndicator()),
                                  //     padding: EdgeInsets.only(bottom: 20),
                                  //   );
                                  // } else {
                                  //   return _headerBanner(_mediaQuery, position);
                                  // }
                                })
                            // return _headerBanner(_mediaQuery),
// LazyLoadScrollView(
//                                 isLoading: _isLoadingList,
//                                 onEndOfPage: () => _loadMore(),
//                                 child:
                            )))));
  }

  _loadingScreen() {
    if (_isLoading) {
      return true;
    }
    // if (_isLoadingSignIn) {
    //   return true;
    // }
    return false;
  }

  Widget _headerBanner(var _mediaQuery, int i) {
    List<Widget> lines = [];

    // for (var i = position; i < data.length; i++) {
    // if (position > _listGSosial.length - 1) {
    //   break;
    // }
    if (data[i]['isShow'] == true) {
      lines.add(Container(
          padding: EdgeInsets.only(bottom: i == data.length - 1 ? 30 : 15),
          child: Container(
              foregroundDecoration: BoxDecoration(
                color: data[i]['isAvailable'] == true
                    ? Colors.transparent
                    : Colors.grey,
                backgroundBlendMode: BlendMode.saturation,
              ),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                color: Colors.white,
                boxShadow: [
                  new BoxShadow(
                    color: Colors.black,
                    offset: Offset(5, 7),
                    blurRadius: 10.0,
                  ),
                ],
              ),
              child: InkWell(
                onTap: () {
                  if (data[i]['isAvailable'] == true) {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => EventOnePage(
                            text: jsonEncode(data[i]),
                          ),
                        ));
                  } else {
                    Widget okButton = FlatButton(
                      child: Text("OK"),
                      onPressed: () => Navigator.pop(context),
                    );
                    showDialog(
                        context: context,
                        builder: (context) {
                          return AlertDialog(
                            content: Container(
                                // margin: EdgeInsets.only(top: 50),
                                child: Text("Event ini sudah tidak tersedia")),
                            actions: <Widget>[okButton],
                          );
                        });
                  }
                },
                child: SizedBox(
                  width: _mediaQuery.size.width - 30,
                  child: Image.network(data[i]['imageBannerName']),
                ),
              ))));
    }

    return Container(
        child: Column(
      children: lines,
    ));
  }

//   Widget _headerBanner(var _mediaQuery) {
//     List<Widget> lines = [];
//     // for (var sizeBox in imgList) {

//     for (var i = 0; i < _listGSosial.length; i++) {
//       if (_listGSosial[i]['isShow'] == true) {
//         lines.add(Container(

//             // padding: EdgeInsets.only(top: i == imgList.length - 1 ? 0 : 15),
//             padding:
//                 EdgeInsets.only(bottom: i == _listGSosial.length - 1 ? 0 : 15),
//             child: Container(
//                 foregroundDecoration: BoxDecoration(
//                   color: _listGSosial[i]['isAvailable'] == true
//                       ? Colors.transparent
//                       : Colors.grey,
//                   backgroundBlendMode: BlendMode.saturation,
//                 ),
//                 decoration: BoxDecoration(
//                   borderRadius: BorderRadius.circular(5),
//                   color: Colors.white,
//                   boxShadow: [
//                     new BoxShadow(
//                       color: Colors.black,
//                       offset: Offset(5, 7),
//                       blurRadius: 10.0,
//                     ),
//                   ],
//                 ),
//                 child: InkWell(
//                   onTap: () {
//                     // print(new DateFormat('dd-M-yyyy').format(
//                     //     DateTime.fromMicrosecondsSinceEpoch(
//                     //         _listGSosial[i]['dateStart'] * 1000)));

//                     // print(_listGSosial[i]);
//                     // String _datas = jsonEncode(_listGSosial[i]);

//                     if (_listGSosial[i]['isAvailable'] == true) {
//                       // Navigator.of(context).pushNamed(UrlPalette.EVENT1);
//                       // Navigator.pushReplacement(
//                       //     context,
//                       //     MaterialPageRoute(
//                       //         builder: (BuildContext context) => EventOnePage(
//                       //               text: "test",
//                       //             )));
//                       // Navigator.pushAndRemoveUntil(
//                       //     context,
//                       //     MaterialPageRoute(
//                       //         builder: (BuildContext context) => EventOnePage(
//                       //               text: "test",
//                       //             )),
//                       //     (Route<dynamic> route) => false);
//                       Navigator.push(
//                           context,
//                           MaterialPageRoute(
//                             builder: (context) => EventOnePage(
//                               text: jsonEncode(_listGSosial[i]),
//                             ),
//                           ));
//                     } else {
//                       Widget okButton = FlatButton(
//                         child: Text("OK"),
//                         onPressed: () => Navigator.pop(context),
//                       );
//                       showDialog(
//                           context: context,
//                           builder: (context) {
//                             return AlertDialog(
//                               content: Container(
//                                   // margin: EdgeInsets.only(top: 50),
//                                   child:
//                                       Text("Event ini sudah tidak tersedia")),
//                               actions: <Widget>[okButton],
//                             );
//                           });
//                     }
//                   },
//                   child: SizedBox(
//                     width: _mediaQuery.size.width - 30,
//                     // child: Text(_listGSosial[i]['imageBannerName']),
//                     child: Image.network(_listGSosial[i]['imageBannerName']),
//                     // child: Image.asset(i > 3 ? imgList[3] : imgList[i]),
//                   ),
//                 ))));
//       }
//     }
//     return Container(
//         child: Column(
//       children: lines,
//     ));
//   }
}
