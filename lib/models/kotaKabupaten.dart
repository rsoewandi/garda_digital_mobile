import 'package:flutter/widgets.dart';

class KotaKabupaten {
  int id;
  String namaKotaKabupaten;

  // KotaKabupaten(this.id, this.namaKotaKabupaten);
  KotaKabupaten({this.id, this.namaKotaKabupaten});

  factory KotaKabupaten.fromJson(Map<String, dynamic> parsedJson) {
    return KotaKabupaten(
        id: parsedJson["id"],
        namaKotaKabupaten: parsedJson["namaKotaKabupaten"] as String);
  }
  // factory KotaKabupaten.fromJson(dynamic json) {
  //   return KotaKabupaten(
  //       json['id'] as String, json['namaKotaKabupaten'] as String);
  // }
}
