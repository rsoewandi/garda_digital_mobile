import 'package:garda_digital/models/kotaKabupaten.dart';

class Provinsi {
  int id;
  String namaProvinsi;
  List<KotaKabupaten> kotaKabupaten;

  Provinsi(this.id, this.namaProvinsi, [this.kotaKabupaten]);

  // factory Provinsi.fromJson(Map<String, dynamic> parsedJson) {
  //   return Provinsi(
  //     id: parsedJson["id"],
  //     namaProvinsi: parsedJson["namaProvinsi"] as String,
  //     kotaKabupaten: parsedJson["kotaKabupaten"] as KotaKabupaten,
  //   );
  // }
  factory Provinsi.fromJson(dynamic json) {
    if (json['kotaKabupaten'] != null) {
      var tagObjsJson = json['kotaKabupaten'] as List;
      List<KotaKabupaten> _tags = tagObjsJson
          .map((tagJson) => KotaKabupaten.fromJson(tagJson))
          .toList();
      return Provinsi(json['id'], json['namaProvinsi'] as String, _tags);
    } else {
      return Provinsi(json['id'], json['namaProvinsi'] as String);
    }
  }
}
