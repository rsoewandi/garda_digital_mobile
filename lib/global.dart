import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:garda_digital/models/provinsi.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class ColorPalette {
  static const primaryColor = Color(0xFFFFFFFF);
  static const primaryDarkColor = Color(0xFFFFFFFF);
  static const underlineTextField = Color(0xFFFFFFFF);
  static const hintColor = Color(0xFFFFFFFF);
  static const iconText = Colors.black;
  static const underLineText = Colors.black;
  static const textColor = Colors.black;
  static const white = Colors.white;
  static const orange = Color.fromRGBO(255, 97, 0, 1);
}

class UrlPalette {
  static const HOME = "/home";
  static const LOGIN = "/login";
  static const REGISTER = "/register";
  static const REGISTER2 = "/register2";
  static const OTP = "/otp";
  static const UPLOAD = "/upload";
  static const CAMERA = "/camera";
  static const WELCOMEPAGE = "/ws1";
  static const DASHBOARD = "/dashboard";
  static const EVENT1 = "/event1";
  static const SOSIALPAGE = "/sosial";
  static const HISTORYPAGE = "/history";
}

class SessionString {
  static const fcmToken = "fcmToken";
  static const fcmOtp = "otpFcm";
  static const otp = "otp";
  static const noTelp = "userId";
  static const token = "token";
}

class DateTimeIndo {
  static Future<String> getDate(var _dates) async {
    var bulan = [
      'Januari',
      'Februari',
      'Maret',
      'April',
      'Mei',
      'Juni',
      'Juli',
      'Agustus',
      'September',
      'Oktober',
      'November',
      'Desember'
    ];
    var splitDate = _dates.split("-");
    return splitDate[0] +
        " " +
        bulan[int.parse(splitDate[1])] +
        " " +
        splitDate[2];
  }
}

class URLS {
  // static const String BASE_URL = 'http://10.17.100.138:9999';
  static const String BASE_URL = 'http://147.139.167.88:19999';
}

class ApiService {
  static Future<List<dynamic>> getDataExample() async {
    // RESPONSE JSON :
    // [{
    //   "id": "1",
    //   "employee_name": "",
    //   "employee_salary": "0",
    //   "employee_age": "0",
    //   "profile_image": ""
    // }]
    final response = await http.get('${URLS.BASE_URL}/employees');
    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      return null;
    }
  }

  static Future<bool> registerApi(body) async {
    // BODY
    // {
    //   "name": "test",
    //   "age": "23"
    // }
    print('${URLS.BASE_URL}/register');
    final response = await http.post('${URLS.BASE_URL}/register',
        headers: {"Content-Type": "application/json"}, body: body);
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  static Future<bool> uploadApi(body) async {
    final response = await http.post('${URLS.BASE_URL}/upload_image',
        headers: {"Content-Type": "application/json"}, body: body);
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  static Future<http.Response> postMapping(url, body) async {
    var _headers = {"Accept": "application/json"};
    // Dio dio = new Dio();
    // dio.options.headers['Content-Type'] = 'application/json';
    // dio.options.headers['Accept'] = 'application/json';
    // final responseDio = await dio.post('${URLS.BASE_URL}' + url, data: body);
    // print(responseDio);
    final response = await http.post('${URLS.BASE_URL}' + url,
        headers: {"Content-Type": "application/json"}, body: jsonEncode(body));

    return response;
  }

  static Future<http.Response> postMappingWithToken(url, body, token) async {
    final response = await http.post('${URLS.BASE_URL}' + url,
        headers: {"Content-Type": "application/json", "Authorization": token},
        body: jsonEncode(body));

    return response;
  }

  static Future<http.Response> getMappingWithToken(url, token) async {
    final response = await http.get('${URLS.BASE_URL}' + url,
        headers: {"Content-Type": "application/json", "Authorization": token});

    return response;
  }

  static Future<List<Provinsi>> findProvinsi() async {
    final response = await http.post('${URLS.BASE_URL}/findProvinsi',
        headers: {"Content-Type": "application/json"}, body: jsonEncode({}));
    if (response.statusCode == 200) {
      return loadProvinsi(response.body);
    } else {
      return null;
    }

    // return parsed.map<Provinsi>((json) => Provinsi.fromJson(json)).toList();
  }

  static List<Provinsi> loadProvinsi(String jsonString) {
    final parsed = json.decode(jsonString).cast<Map<String, dynamic>>();
    return parsed.map<Provinsi>((json) => Provinsi.fromJson(json)).toList();
  }
}

class SessionLogin {
  static Future<bool> cek() async {
    SharedPreferences _session = await SharedPreferences.getInstance();
    var tokenLogin = _session.get(SessionString.token);
    if (tokenLogin == null) {
      return false;
    }
    if (tokenLogin == "") {
      return false;
    }
    return true;
  }
}

class MySelectionItem extends StatelessWidget {
  final String title;
  final bool isForList;

  const MySelectionItem({Key key, this.title, this.isForList = true})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 60.0,
      child: isForList
          ? Padding(
              child: _buildItem(context),
              padding: EdgeInsets.all(10.0),
            )
          : Card(
              margin: EdgeInsets.symmetric(horizontal: 10.0),
              child: Stack(
                children: <Widget>[
                  _buildItem(context),
                  Align(
                    alignment: Alignment.centerRight,
                    child: Icon(Icons.arrow_drop_down),
                  )
                ],
              ),
            ),
    );
  }

  _buildItem(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      alignment: Alignment.center,
      child: Text(title),
    );
  }
}
