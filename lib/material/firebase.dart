import 'package:flutter/material.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:garda_digital/global.dart';
import 'dart:io';

import 'package:shared_preferences/shared_preferences.dart';

class firebase {
  final firebaseMessaging = FirebaseMessaging();
  bool _started = false;

  // firebase._internal();
  // static final firebase instance = firebase._internal();

  static String dataName = '';
  static String dataAge = '';

  // void start() {
  //   if (!_started) {
  //     _start();
  //     _started = true;
  //     _refreshToken();
  //   }
  // }

  // void _refreshToken() {
  //   _firebaseMessaging.getToken().then(_tokenRefresh, onError: _tokenRefreshFailure);
  // }
  _tokenFcm(var token) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(SessionString.fcmToken, token);
  }

  _setOtp(var otp) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(SessionString.fcmOtp, otp);
  }

  firebaseNotification(context) {
    firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        debugPrint('onMessage: $message');
        getDataFcm(message);
      },
      onBackgroundMessage: onBackgroundMessage,
      onResume: (Map<String, dynamic> message) async {
        debugPrint('onResume: $message');
        getDataFcm(message);
      },
      onLaunch: (Map<String, dynamic> message) async {
        debugPrint('onLaunch: $message');
        getDataFcm(message);
      },
    );
    firebaseMessaging.requestNotificationPermissions(
      const IosNotificationSettings(
          sound: true, badge: true, alert: true, provisional: true),
    );
    firebaseMessaging.onIosSettingsRegistered.listen((settings) {
      debugPrint('Settings registered: $settings');
    });
//    firebaseMessaging.getToken().then((token) =>
//        setState(() {
//          this.token = token;
//        }));
    firebaseMessaging.getToken().then((token) {
      debugPrint('this is my token : ' + token);
      Map<String, dynamic> message = new Map();

      message['name'] = token;
      _tokenFcm(token);
      // final notification = LocalNotification("fcmToken", message as Map);
      // NotificationsBloc.instance.newNotification(notification);
    });
  }

  static Future<dynamic> onBackgroundMessage(
      Map<String, dynamic> message) async {
    debugPrint('onBackgroundMessage: $message');
    if (message.containsKey('data')) {
      String name = '';
      String age = '';
      if (Platform.isIOS) {
        name = message['name'];
        age = message['age'];
      } else if (Platform.isAndroid) {
        var data = message['data'];
        name = data['name'];
        age = data['age'];
      }
      dataName = name;
      dataAge = age;
      debugPrint('onBackgroundMessage: name: $name & age: $age');
    }
    return null;
  }

  void getDataFcm(Map<String, dynamic> message) {
    print(message['data']['mobileNumber']);
    print(message['data']['otpCode']);
    String mobileNumber = '';
    String otpCode = '';
    if (Platform.isIOS) {
      mobileNumber = message['mobileNumber'];
      otpCode = message['otpCode'];
    } else if (Platform.isAndroid) {
      var data = message['data'];
      mobileNumber = data['mobileNumber'];
      otpCode = data['otpCode'];
      _setOtp(otpCode);
    }
  }
}
