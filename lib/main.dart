import 'package:flutter/material.dart';
import 'package:garda_digital/global.dart';
import 'package:garda_digital/screens/dashboard.dart';
import 'package:garda_digital/screens/history/history_page.dart';
import 'package:garda_digital/screens/login_view.dart';
import 'package:garda_digital/screens/otp_view.dart';
import 'package:garda_digital/screens/register_2_view.dart';
import 'package:garda_digital/screens/register_view.dart';
import 'package:garda_digital/screens/sosial/l_sosial.dart';
import 'package:garda_digital/screens/welcome_view.dart';
import 'package:garda_digital/screens/event/event1.dart';

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    title: "Garda Digital",
    theme: ThemeData(fontFamily: 'Montserrat'),
    initialRoute: UrlPalette.WELCOMEPAGE,
    routes: {
      UrlPalette.DASHBOARD: (BuildContext context) => new DashboardPage(),
      UrlPalette.LOGIN: (BuildContext context) => new LoginPage(),
      UrlPalette.REGISTER: (BuildContext context) =>
          new RegisterPage(text: "test"),
      UrlPalette.REGISTER2: (BuildContext context) =>
          new RegisterPage2(text: null),
      UrlPalette.WELCOMEPAGE: (BuildContext context) => new WelcomePage(),
      UrlPalette.OTP: (BuildContext context) => new OtpPage(),
      UrlPalette.EVENT1: (BuildContext context) => new EventOnePage(),
      UrlPalette.SOSIALPAGE: (BuildContext context) => new SosialPage(),
      UrlPalette.HISTORYPAGE: (BuildContext context) => new HistoryPage(),
    },
  ));
}
